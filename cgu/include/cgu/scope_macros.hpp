
/*
BSD 3-Clause License

Copyright (c) 2020-2021, Benjamin Allen
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#pragma once

#ifndef CGU_SCOPE_MACROS_HPP
#define CGU_SCOPE_MACROS_HPP

#include "macros.hpp"
#include "scope_macros_base.hpp"

#include "scope.hpp"

#define CGU_SCOPE_INFO_NAMESPACE_IMPL(name, parent_tag) \
	CGU_WARNING_PUSH; \
	CGU_WARNING_DISABLE_SHADOW; \
	/* templated and specialized by parent tag at namespace scope for linkage uniqueness.
	* the base template is always the same for ODR reasons, so we use an alias for the actual tag type. */ \
	template <typename> struct CGU_TOKENPASTE(_cgu_scope_tag_impl_, name) {}; \
	CGU_DEFINE_SCOPE_TAG(template <> struct CGU_TOKENPASTE(_cgu_scope_tag_impl_, name)<parent_tag>, name, parent_tag, ""); \
	using CGU_SCOPE_TAG(name) = CGU_TOKENPASTE(_cgu_scope_tag_impl_, name)<parent_tag>; \
	using _cgu_scope_tag = CGU_SCOPE_TAG(name); \
	CGU_WARNING_POP

// declare namespace scope info with explicit parent
#define CGU_SCOPE_INFO_NAMESPACE_PARENT(name, parent) CGU_SCOPE_INFO_NAMESPACE_IMPL(name, CGU_SCOPE_TAG(parent))
// declare namespace scope info with implicit parent
#define CGU_SCOPE_INFO_NAMESPACE(name) CGU_SCOPE_INFO_NAMESPACE_IMPL(name, _cgu_scope_tag)

// declare (but do not define) class scope info.
// unless a definition of the scope info is available, the scope tag is undefined
// and so dependent functionality will also be unavailable (such as declaring child scopes).
#define CGU_EXTERN_SCOPE_INFO_CLASS(name) \
	CGU_WARNING_PUSH; \
	CGU_WARNING_DISABLE_SHADOW; \
	struct CGU_SCOPE_TAG(name); \
	using _cgu_scope_tag = CGU_SCOPE_TAG(name); \
	CGU_WARNING_POP

#define CGU_DEFINE_SCOPE_INFO_CLASS_IMPL(cls, name, parent_tag) \
	using CGU_TOKENPASTE(_cgu_scope_tag_parent_, name) = parent_tag; \
	CGU_DEFINE_SCOPE_TAG(struct cls::CGU_SCOPE_TAG(name), name, CGU_TOKENPASTE(_cgu_scope_tag_parent_, name), "")

// define class scope info with explicit parent (out-of-class)
#define CGU_DEFINE_SCOPE_INFO_CLASS_PARENT(cls, name, parent) CGU_DEFINE_SCOPE_INFO_CLASS_IMPL(cls, name, CGU_SCOPE_TAG(parent))
// define class scope info with implicit parent (out-of-class)
#define CGU_DEFINE_SCOPE_INFO_CLASS(cls, name) CGU_DEFINE_SCOPE_INFO_CLASS_IMPL(cls, name, _cgu_scope_tag)

#define CGU_SCOPE_INFO_IMPL(name, parent_tag) \
	CGU_WARNING_PUSH; \
	CGU_WARNING_DISABLE_SHADOW; \
	constexpr static std::string_view _cgu_func_str = CGU_FUNC_STR; \
	CGU_DEFINE_SCOPE_TAG(struct CGU_SCOPE_TAG(name), name, parent_tag, _cgu_func_str); \
	using _cgu_scope_tag = CGU_SCOPE_TAG(name); \
	CGU_WARNING_POP

// declare block (function) scope info with explicit parent
#define CGU_SCOPE_INFO_PARENT(name, parent) CGU_SCOPE_INFO_IMPL(name, CGU_SCOPE_TAG(parent))
// declare block (function) scope info with implicit parent
#define CGU_SCOPE_INFO(name) CGU_SCOPE_INFO_IMPL(name, _cgu_scope_tag)

// note: _cgu_scope_info is no longer declared to avoid linkage concerns
#define CGU_CURRENT_SCOPE (::cgu::detail::scope_by_tag<_cgu_scope_tag>)

#endif
