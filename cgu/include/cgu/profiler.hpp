
/**
BSD 3-Clause License

Copyright (c) 2020, Benjamin Allen
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#pragma once

#ifndef CGU_PROFILER_HPP
#define CGU_PROFILER_HPP

#include <cassert>
#include <cstdint>
#include <string>
#include <utility>
#include <vector>
#include <thread>
#include <chrono>
#include <mutex>
#include <memory>
#include <optional>
#include <unordered_map>

#include "scope.hpp"
#include "opengl.hpp"

namespace cgu {

	// forward declarations
	class profiler;
	class profile_frame;
	class profiler_gpu_resources;

	enum class profile_flags : unsigned char {
		none = 0,
		// enable cpu profiling
		cpu = 0x1,
		// enable gpu profiling
		gpu = 0x2,
		// cpu and gpu
		cpu_gpu = 0x3,
		// disable profiling of child sections
		disable = 0x4
	};

	constexpr profile_flags operator|(profile_flags a, profile_flags b) {
		return profile_flags{unsigned(a) | unsigned(b)};
	}

	constexpr profile_flags operator&(profile_flags a, profile_flags b) {
		return profile_flags{unsigned(a) & unsigned(b)};
	}

	constexpr profile_flags operator~(profile_flags a) {
		return profile_flags{~unsigned(a)};
	}

	constexpr bool operator!(profile_flags a) {
		return !unsigned(a);
	}

	template <profile_flags DefFlags = profile_flags::none, typename ...Ts>
	constexpr profile_flags make_profile_flags(const Ts &...args) {
		return (DefFlags | ... | profile_flags{args});
	}

	// profile section declaration
	struct static_profile_opts {
		const static_scope_info *scope = nullptr;
		profile_flags flags{};

		constexpr bool test(profile_flags f) const noexcept {
			return (flags & f) == f;
		}

		constexpr void set(profile_flags f, bool on = true) noexcept {
			if (on) {
				flags = flags | f;
			} else {
				flags = flags & ~f;
			}
		}
	};

	namespace detail {

		struct profile_guard_data {
			const profile_guard_data *m_parent = nullptr;
			static_profile_opts *m_section = nullptr;
			profile_frame *m_frame = nullptr;
			// need to use index instead of pointer as the vector gets resized
			size_t m_frame_index = 0;
			// enable for benefit of root section
			bool m_enable_profile_children = true;
		};

	}

	// RAII profile section entry/exit
	// must only be instantiated as a local variable.
	class profile_guard : private detail::profile_guard_data {
	public:
		~profile_guard();

		profile_guard(const profile_guard &) = delete;
		profile_guard & operator=(const profile_guard &) = delete;

		explicit profile_guard(static_profile_opts *) noexcept;

		const static_profile_opts * section() const noexcept {
			return m_section;
		}

		const profile_guard * parent() const noexcept {
			return static_cast<const profile_guard *>(m_parent);
		}

		// thread-local current profile section
		static const profile_guard * current() noexcept;

		// global root profile section
		static const profile_guard * root() noexcept;
	};

	// clock definition for std::chrono to represent time_points retrieved from the gpu
	struct gpu_clock {
		using rep = long long;
		using period = std::nano;
		using duration = std::chrono::nanoseconds;
		using time_point = std::chrono::time_point<gpu_clock>;
		static constexpr bool is_steady = true;

		// now() is not implementable
	};

	// section profiling statistics
	struct section_stats {
		static_profile_opts *section = nullptr;
		// note - profile stats parent is not necessarily the same as section guard parent
		size_t parent = 0;
		std::thread::id tid{};
		profile_flags flags{};
		// note - profile frame section depth, not absolute section depth
		int depth = 0;
		int descendants = 0;
		std::chrono::steady_clock::time_point time_cpu_enter{};
		std::chrono::steady_clock::time_point time_cpu_exit{};
		gpu_clock::time_point time_gpu_enter{};
		gpu_clock::time_point time_gpu_exit{};
		GLuint gpu_query_enter = 0;
		GLuint gpu_query_exit = 0;
	};

	struct profile_frame_stats {
		profile_flags flags{};
		std::chrono::steady_clock::time_point time_cpu_enter{};
		std::chrono::steady_clock::time_point time_cpu_exit{};
		gpu_clock::time_point time_gpu_enter{};
		gpu_clock::time_point time_gpu_exit{};
		std::chrono::nanoseconds duration_cpu{};
		std::chrono::nanoseconds duration_gpu{};
	};
	
	// identifies a series of profile frames
	// must outlive all related profile frames.
	struct profile_task {
		// note - the callback does not receive ownership of the frame
		using callback_t = void(*)(const profile_task *, const profile_frame &);
		std::string name;
		// called when the frame is ready (from possibly any thread)
		callback_t callback = nullptr;
	};

	struct profile_frame_gpu_queries {
		profiler_gpu_resources *gpures = nullptr;
		profile_frame *frame = nullptr;
		size_t index = 0;
	};

	// must outlive any related profile frames. not movable (stable address).
	// not threadsafe; must not be shared between threads (even between contexts that share gpu resources).
	class profiler_gpu_resources {
	private:
		// we allocate a pool of query objects upfront
		// TODO how to set this nicely?
		static constexpr GLsizei max_queries = 2048;
		std::vector<GLuint> m_queries;
		std::pair<std::chrono::steady_clock::time_point, gpu_clock::time_point> m_clock_sync;
		GLuint m_post_present_query = 0;
		GLsync m_pre_present_sync = nullptr;
		std::chrono::steady_clock::time_point m_next_present;

	public:
		~profiler_gpu_resources();

		profiler_gpu_resources();

		profiler_gpu_resources(const profiler_gpu_resources &) = delete;
		profiler_gpu_resources & operator=(const profiler_gpu_resources &) = delete;

		auto clock_sync() const noexcept {
			return m_clock_sync;
		}

		// use (only) in windowed mode to try wait until a suitable presentation time
		// to sync to desktop composition timing. call every frame, after pre_present()
		// and before actually presenting. mimics nvidia's adaptive vsync.
		// use with vsync disabled.
		bool try_wait_desktop_sync() noexcept;

		// call before presenting each frame (eg SwapBuffers).
		// tries automatic low-impact clock sync.
		void pre_present() noexcept;

		// call after presenting each frame (eg SwapBuffers).
		// tries automatic low-impact clock sync.
		void post_present() noexcept;

		// update cpu-gpu clock sync with full sync point.
		// very bad for performance, do not call frequently.
		void update_clock_sync() noexcept;

		void _enter(section_stats &stats) noexcept;

		void _exit(section_stats &stats) noexcept;

		bool _ready(const section_stats &stats) noexcept;

		void _retrieve(section_stats &stats) noexcept;

		void _bind(profile_frame *frame);

		void _unbind(profile_frame *frame) noexcept;

	private:
		GLuint alloc_query() noexcept {
			if (m_queries.empty()) return 0;
			GLuint q = m_queries.back();
			m_queries.pop_back();
			return q;
		}

		void free_query(GLuint q) noexcept {
			if (!q) return;
			m_queries.push_back(q);
		}
	};

	class profile_frame {
		friend class profiler;
	private:
		profiler *m_prof = nullptr;
		const profile_task *m_task = nullptr;
		std::vector<section_stats> m_stats;
		profile_frame *m_current_parent = nullptr;
		profiler_gpu_resources *m_current_gpures = nullptr;
		std::thread::id m_current_tid;
		profile_flags m_current_enable{};
		size_t m_current_bind_index = 0;
		size_t m_current_bind_depth = 0;

		int m_outstanding_gpuqueries = 0;
		std::pair<std::chrono::steady_clock::time_point, gpu_clock::time_point> m_clock_sync;
		bool m_closed = false;
		bool m_ready = false;

	public:
		profile_frame() = default;

		explicit profile_frame(profiler *prof, const profile_task *task);

		// bind this frame to the current thread
		// it is possible to bind over the top of another frame.
		// gpu profiling requires a gpu resources instance, which must remain
		// current/valid until the profile frame is unbound.
		void bind(profile_flags enable, profiler_gpu_resources *gpures = nullptr);

		// unbind this frame from the current thread
		void unbind() noexcept;

		// is this frame bound to any thread?
		bool bound() const noexcept {
			return m_current_tid != std::thread::id{};
		}

		// is all profile data available?
		bool ready() const noexcept {
			return m_ready;
		}

		// access section profile data
		// only when frame is ready().
		const std::vector<section_stats> & stats() const noexcept {
			assert(m_ready);
			return m_stats;
		}

		// combined stats for every time this frame was bound
		profile_frame_stats combined_stats() const noexcept;

		auto clock_sync() const noexcept {
			return m_clock_sync;
		}

		profiler * prof() const noexcept {
			return m_prof;
		}

		const profile_task * task() const noexcept {
			return m_task;
		}

		size_t _enter(size_t parent, static_profile_opts *section) noexcept;

		void _exit(size_t index) noexcept;

		size_t _bind_index() const noexcept {
			return m_current_bind_index;
		}

		// thread-local current frame
		static profile_frame * current() noexcept;

	private:
		void enter_timestamp(size_t index) noexcept;
		void exit_timestamp(size_t index) noexcept;
	};

	// must outlive any related active/pending profile frames. not movable (stable address).
	class profiler {
	private:
		// most frames owned by the profiler at once
		// prevents explosive memory use if user forgets to poll()
		static constexpr size_t max_frames = 64;
		std::mutex m_mtx;
		// frame address must be stable while active/pending, hence unique_ptr
		std::vector<std::unique_ptr<profile_frame>> m_frames;
		std::vector<profile_frame_gpu_queries> m_gpuqueries;

	public:
		profiler();

		profile_frame * new_frame(const profile_task *task);

		void end_frame(profile_frame *) noexcept;

		// receive the next frame whose profile data is ready
		// user should poll() the frames frequently
		std::optional<profile_frame> poll() noexcept;

		void _bind(profile_frame *frame);

		void _unbind(profile_frame *frame) noexcept;

	private:
		void tick(profiler_gpu_resources *, const std::unique_lock<std::mutex> &) noexcept;
	};

	// RAII profile_frame activation helper (ends the frame on destruction)
	// can be moved between threads.
	class profile_frame_activation {
	private:
		profile_frame *m_frame = nullptr;

	public:
		~profile_frame_activation() {
			destroy();
		}

		profile_frame_activation() = default;

		profile_frame_activation(const profile_frame_activation &) = delete;
		profile_frame_activation & operator=(const profile_frame_activation) = delete;

		profile_frame_activation(profile_frame_activation &&other) noexcept {
			m_frame = other.m_frame;
			other.m_frame = nullptr;
		}

		profile_frame_activation & operator=(profile_frame_activation &&other) noexcept {
			destroy();
			m_frame = other.m_frame;
			other.m_frame = nullptr;
			return *this;
		}

		explicit profile_frame_activation(profiler &prof, const profile_task &task) {
			m_frame = prof.new_frame(&task);
		}

		profile_frame * frame() const noexcept {
			return m_frame;
		}

	private:
		void destroy() noexcept {
			if (m_frame) m_frame->prof()->end_frame(m_frame);
		}
	};

	// RAII profile_frame bind helper (unbinds the frame on destruction)
	// must not be passed between threads.
	class profile_frame_binding {
	private:
		profile_frame *m_frame = nullptr;

	public:
		~profile_frame_binding() {
			destroy();
		}

		profile_frame_binding() = default;

		profile_frame_binding(const profile_frame_binding &) = delete;
		profile_frame_binding & operator=(const profile_frame_binding &) = delete;

		profile_frame_binding(profile_frame_binding &&other) noexcept {
			m_frame = other.m_frame;
			other.m_frame = nullptr;
		}

		profile_frame_binding & operator=(profile_frame_binding &&other) noexcept {
			destroy();
			m_frame = other.m_frame;
			other.m_frame = nullptr;
			return *this;
		}

		// gpu profiling requires a gpu resources instance, which must remain
		// current/valid until the profile frame is unbound.
		explicit profile_frame_binding(const profile_frame_activation &active, profile_flags enable, profiler_gpu_resources *gpures = nullptr) : m_frame(active.frame()) {
			if (m_frame) m_frame->bind(enable, gpures);
		}

	private:
		void destroy() noexcept {
			if (m_frame) m_frame->unbind();
		}
	};

	struct profile_collector {
		struct task_data {
			const profile_task *task = nullptr;
			std::chrono::nanoseconds expected_duration = std::chrono::seconds(1);
			// selected frame, backwards from the end (0 = most recent)
			int selection = 0;
			std::vector<profile_frame> frames;
			bool paused = false;
		};

		profiler *prof = nullptr;
		size_t max_frames = 144;
		const profile_task *selected_task = nullptr;
		std::unordered_map<const profile_task *, task_data> data;
		bool want_clock_sync = false;

		void poll() {
			if (prof) push(prof->poll());
		}

		void push(profile_frame &&frame) {
			auto &d = data[frame.task()];
			d.task = frame.task();
			if (d.paused) return;
			auto &v = d.frames;
			v.push_back(std::move(frame));
			if (v.size() >= max_frames) {
				auto i = v.size() - max_frames;
				v.erase(v.begin(), v.begin() + i);
			}
		}

		void push(std::optional<profile_frame> &&frame) {
			if (frame.has_value()) push(std::move(frame).value());
		}
	};

}

#ifndef CGU_NO_IMGUI
namespace ImGui {
	void draw_profile_frame(const ::cgu::profile_frame &frame, ::std::chrono::nanoseconds expected_duration, float &scroll_origin, float &scroll_extent);
	void draw_profile_collector(::cgu::profile_collector &collector);
	void draw_profile_lagometer(::cgu::profile_collector::task_data &task);
}
#endif

#endif
