
/*
BSD 3-Clause License

Copyright (c) 2020-2021, Benjamin Allen
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#pragma once

#ifndef CGU_SCOPE_MACROS_BASE_HPP
#define CGU_SCOPE_MACROS_BASE_HPP

#ifndef CGU_SCOPE_MACROS_HPP
#error do not include this header directly (include scope_macros.hpp)
#endif

#include "macros.hpp"

#define CGU_SCOPE_TAG(name_) CGU_TOKENPASTE(_cgu_scope_tag_, name_)
#define CGU_SCOPE_BY_TAG(name_) (::cgu::detail::scope_by_tag<CGU_SCOPE_TAG(name_)>)

#define CGU_DEFINE_SCOPE_TAG(prelude_, name_, parent_tag_, func_) \
	/* this struct gets declared locally so cannot have static data members (even constexpr ones).
	* the use of functions also serves as a workaround in msvc for properly capturing
	* shadowed variables. */ \
	prelude_ { \
	using parent_tag = parent_tag_; \
	static constexpr const ::cgu::static_scope_info * parent() { return &::cgu::detail::scope_by_tag<parent_tag>; } \
	static constexpr ::std::string_view name() { return CGU_STRINGIFY(name_); } \
	static constexpr ::std::string_view func() { constexpr ::std::string_view s = func_; return s; } \
	static constexpr ::std::string_view file() { return __FILE__; } \
	static constexpr int line() { return __LINE__; } }

// define a local scope root (for e.g. a client library) at namespace scope.
// user must ensure the name is unique for linkage purposes.
// does not become an implicit parent.
#define CGU_DEFINE_SCOPE_ROOT(name) CGU_DEFINE_SCOPE_TAG(struct CGU_SCOPE_TAG(name), name, ::cgu::scope_tag_global_root, "")

#endif
