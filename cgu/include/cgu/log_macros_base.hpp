
/*
BSD 3-Clause License

Copyright (c) 2020-2021, Benjamin Allen
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#pragma once

#ifndef CGU_LOG_MACROS_BASE_HPP
#define CGU_LOG_MACROS_BASE_HPP

#ifndef CGU_LOG_MACROS_HPP
#error do not include this header directly (include log_macros.hpp)
#endif

#include "macros.hpp"

// define a log category. use only at global scope.
// defines a function parameterized by the scope tag for linkage uniqueness.
#define CGU_DEFINE_LOG_CATEGORY(scope_, name_, verbosity_, severity_, ...) \
	namespace cgu::logcat { \
	constexpr ::cgu::log_category name_(decltype(CGU_SCOPE_TAG(scope_){}) *) { \
	::cgu::log_category cat{&::cgu::detail::scope_by_tag<decltype(CGU_SCOPE_TAG(scope_){})>, \
		CGU_STRINGIFY(name_), (verbosity_), (::cgu::log_severity::severity_) }; \
	__VA_ARGS__; \
	return cat; } }

#endif
