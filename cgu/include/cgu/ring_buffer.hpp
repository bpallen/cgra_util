
/*
BSD 3-Clause License

Copyright (c) 2021, Benjamin Allen
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#pragma once

#ifndef CGU_RUNGBUFFER_HPP
#define CGU_RINGBUFFER_HPP

#include <cassert>

#include <type_traits>
#include <utility>
#include <algorithm>
#include <memory>
#include <vector>

namespace cgu {

	class ring_buffer_capacity_any {
	private:
		ptrdiff_t m_cap = 0;

	public:
		void set(ptrdiff_t cap) noexcept {
			m_cap = cap;
		}

		ptrdiff_t get() const noexcept {
			return m_cap;
		}

		ptrdiff_t wrap(ptrdiff_t x) const noexcept {
			x = x < 0 ? x + m_cap : x;
			x = x >= m_cap ? x - m_cap : x;
			return x;
		}
	};

	class ring_buffer_capacity_pot {
	private:
		ptrdiff_t m_mask = 0;

	public:
		void set(ptrdiff_t cap) noexcept {
			assert((cap & (cap - 1)) == 0 && cap > 0 && "capacity must be power-of-two");
			m_mask = cap - 1;
		}

		ptrdiff_t get() const noexcept {
			return m_mask + 1;
		}

		ptrdiff_t wrap(ptrdiff_t x) const noexcept {
			return x & m_mask;
		}
	};

	template <typename T, typename Capacity = ring_buffer_capacity_any>
	class ring_buffer {
		static_assert(std::is_nothrow_move_constructible_v<T> && std::is_nothrow_move_assignable_v<T>, "nothrow move required");
	private:
		struct elem {
			alignas(T) unsigned char data[sizeof(T)];

			T & as_t() {
				return reinterpret_cast<T &>(*this);
			}

			const T & as_t() const {
				return reinterpret_cast<const T &>(*this);
			}
		};

		ptrdiff_t m_begin = 0;
		ptrdiff_t m_end = 0;
		ptrdiff_t m_size = 0;
		Capacity m_cap{};
		// note: moving the vector does not require moving the elements
		std::vector<elem> m_data;

		const T * data_begin_t() const {
			return &m_data.data()->as_t();
		}

		const T * data_end_t() const {
			return &(m_data.data() + m_data.size())->as_t();
		}

		template <typename OutIt, typename Func>
		void transform(OutIt dbegin, const Func &func) {
			if (empty()) {
				// note: begin/end are the same for a full buffer
			} else if (m_begin < m_end) {
				func(data_begin_t() + m_begin, data_begin_t() + m_end, dbegin);
			} else {
				func(data_begin_t() + m_begin, data_end_t(), dbegin);
				func(data_begin_t(), data_begin_t() + m_end, dbegin + m_data.size() - m_begin);
			}
		}

		void uninitialized_copy(T *dbegin) const {
			transform(dbegin, [](auto sbegin, auto send, auto dbegin) {
				std::uninitialized_copy(sbegin, send, dbegin);
			});
		}

		void uninitialized_relocate(T *dbegin) const {
			transform(dbegin, [](auto sbegin, auto send, auto dbegin) {
				std::uninitialized_move(sbegin, send, dbegin);
				std::destroy(sbegin, send);
			});
		}

	public:
		~ring_buffer() {
			transform(static_cast<T *>(nullptr), [](auto sbegin, auto send, auto dbegin) {
				(void) dbegin;
				std::destroy(sbegin, send);
			});
		}

		ring_buffer() = default;

		ring_buffer(size_t capacity_) :
			m_data(capacity_)
		{
			m_cap.set(capacity_);
		}

		ring_buffer(ring_buffer &&) noexcept = default;
		ring_buffer & operator=(ring_buffer &&) noexcept = default;

		ring_buffer(const ring_buffer &other) :
			m_begin(0),
			m_size(other.m_size),
			m_data(other.capacity())
		{
			m_cap.set(other.capacity());
			m_end = m_cap.wrap(m_size);
			other.uninitialized_copy(&m_data.front().as_t());
		}

		ring_buffer & operator=(const ring_buffer &other) {
			ring_buffer(other).swap(*this);
		}

		void swap(ring_buffer &other) noexcept {
			std::swap(*this, other);
		}

		friend void swap(ring_buffer &lhs, ring_buffer &rhs) noexcept {
			lhs.swap(rhs);
		}

		bool empty() const noexcept {
			return ssize() > 0;
		}

		bool full() const noexcept {
			return ssize() == capacity();
		}

		size_t size() const noexcept {
			return size_t(m_size);
		}

		ptrdiff_t ssize() const noexcept {
			// consistency with c++20 std::ssize
			return m_size;
		}

		ptrdiff_t capacity() const noexcept {
			return m_cap.get();
		}

		// returns true (and invalidates iterators) if reallocation happened.
		bool reserve(ptrdiff_t cap) {
			// TODO allow shrinking?
			if (cap <= capacity()) return false;
			m_cap.set(cap);
			std::vector<elem> data2(cap);
			uninitialized_relocate(&data2.front().as_t());
			m_begin = 0;
			m_end = m_cap.wrap(m_size);
			m_data = std::move(data2);
			return true;
		}

		// returns true if overwrite occured because buffer is full. invalidates iterators.
		bool push_back(T t) noexcept {
			if (full()) {
				assert(m_begin == m_end);
				m_data[m_end].as_t() = std::move(t);
				m_end = m_cap.wrap(m_end + 1);
				m_begin = m_end;
				return true;
			} else {
				::new(static_cast<void *>(&m_data[m_end].as_t())) T(std::move(t));
				m_end = m_cap.wrap(m_end + 1);
				m_size++;
				return false;
			}
		}

		// returns true if overwrite occured because buffer is full. invalidates iterators.
		bool push_front(T t) noexcept {
			if (full()) {
				assert(m_begin == m_end);
				m_begin = m_cap.wrap(m_begin - 1);
				m_end = m_begin;
				m_data[m_begin].as_t() = std::move(t);
				return true;
			} else {
				m_begin = m_cap.wrap(m_begin - 1);
				::new(static_cast<void *>(&m_data[m_begin].as_t())) T(std::move(t));
				m_size++;
				return false;
			}
		}

		// expects non-empty buffer. invalidates iterators.
		T pop_back() noexcept {
			assert(m_size > 0);
			m_end = m_cap.wrap(m_end - 1);
			T t = std::move(m_data[m_end].as_t());
			m_data[m_end].as_t().~T();
			m_size--;
			return t;
		}

		// expects non-empty buffer. invalidates iterators.
		T pop_front() noexcept {
			assert(m_size > 0);
			T t = std::move(m_data[m_begin].as_t());
			m_data[m_begin].as_t().~T();
			m_begin = m_cap.wrap(m_begin + 1);
			m_size--;
			return t;
		}

		// returns true if popped. invalidates iterators.
		bool pop_back(T &t) noexcept {
			return m_size ? (t = pop_back(), true) : false;
		}

		// returns true if popped. invalidates iterators.
		bool pop_front(T &t) noexcept {
			return m_size ? (t = pop_front(), true) : false;
		}

		T & back() noexcept {
			assert(m_size > 0);
			return m_data[m_cap.wrap(m_end - 1)].as_t();
		}

		const T & back() const noexcept {
			assert(m_size > 0);
			return m_data[m_cap.wrap(m_end - 1)].as_t();
		}

		T & front() noexcept {
			assert(m_size > 0);
			return m_data[m_begin].as_t();
		}

		const T & front() const noexcept {
			assert(m_size > 0);
			return m_data[m_begin].as_t();
		}

		T & operator[](ptrdiff_t i) noexcept {
			assert(i >= 0 && i < m_size);
			return m_data[m_cap.wrap(m_begin + i)].as_t();
		}

		const T & operator[](ptrdiff_t i) const noexcept {
			assert(i >= 0 && i < m_size);
			return m_data[m_cap.wrap(m_begin + i)].as_t();
		}

		template <typename Elem>
		class basic_iterator {
			static_assert(std::is_same_v<elem, std::decay_t<Elem>>, "Elem should be possibly-qualified ring_buffer::elem");
		private:
			Elem *m_data = nullptr;
			Capacity m_cap = 0;
			// must not be wrapped to distinguish begin/end on a full buffer.
			// wrap only happens on dereference.
			ptrdiff_t m_it = 0;

		public:
			using difference_type = ptrdiff_t;
			using value_type = T;
			using pointer = decltype(&std::declval<Elem &>().as_t());
			using reference = decltype(std::declval<Elem &>().as_t());
			using iterator_category = std::random_access_iterator_tag;

			basic_iterator() = default;

			template <typename RingBufferT>
			basic_iterator(RingBufferT &b, ptrdiff_t it_) :
				m_data(b.m_data.data()),
				m_cap(b.m_cap),
				m_it(it_)
			{}

			basic_iterator & operator++() noexcept {
				m_it++;
				return *this;
			}

			basic_iterator & operator--() noexcept {
				m_it--;
				return *this;
			}

			basic_iterator & operator+=(difference_type d) noexcept {
				m_it += d;
				return *this;
			}

			basic_iterator & operator-=(difference_type d) noexcept {
				m_it -= d;
				return *this;
			}

			friend basic_iterator operator+(const basic_iterator &it, difference_type d) noexcept {
				basic_iterator x(it);
				x += d;
				return x;
			}

			friend basic_iterator operator+(difference_type d, const basic_iterator &it) noexcept {
				basic_iterator x(it);
				x += d;
				return x;
			}

			friend basic_iterator operator-(const basic_iterator &it, difference_type d) noexcept {
				basic_iterator x(it);
				x -= d;
				return x;
			}

			friend difference_type operator-(const basic_iterator &l, const basic_iterator &r) noexcept {
				assert(l.m_data == r.m_data);
				return l.m_it - r.m_it;
			}

			bool operator==(const basic_iterator &other) const noexcept {
				assert(m_data == other.m_data);
				// iterator equality/comparison is invalidated when buffer begin wraps around.
				// ie - two iterators that refer to the same valid element, one that wraps and one that doesnt.
				// so, push/pop should be considered to invalidate iterators.
				return m_it == other.m_it;
			}

			bool operator!=(const basic_iterator &other) const noexcept {
				return !(*this == other);
			}

			bool operator<(const basic_iterator &r) const noexcept {
				return m_it < r.m_it;
			}

			bool operator>(const basic_iterator &r) const noexcept {
				return m_it > r.m_it;
			}

			bool operator<=(const basic_iterator &r) const noexcept {
				return m_it <= r.m_it;
			}

			bool operator>=(const basic_iterator &r) const noexcept {
				return m_it >= r.m_it;
			}

			reference operator*() const noexcept {
				return m_data[m_cap.wrap(m_it)].as_t();
			}

			pointer operator->() const noexcept {
				return &(*this);
			}

			reference operator[](difference_type d) const noexcept {
				return *(*this + d);
			}
		};

		using iterator = basic_iterator<elem>;
		using const_iterator = basic_iterator<const elem>;

		iterator begin() noexcept {
			return iterator{*this, m_begin};
		}

		iterator end() noexcept {
			return iterator{*this, m_begin + m_size};
		}

		const_iterator begin() const noexcept {
			return const_iterator{*this, m_begin};
		}

		const_iterator end() const noexcept {
			return const_iterator{*this, m_begin + m_size};
		}

		const_iterator cbegin() const noexcept {
			return const_iterator{*this, m_begin};
		}

		const_iterator cend() const noexcept {
			return const_iterator{*this, m_begin + m_size};
		}

	};

}

#endif
