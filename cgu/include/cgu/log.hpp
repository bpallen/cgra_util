
/*
BSD 3-Clause License

Copyright (c) 2021, Benjamin Allen
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#pragma once

#ifndef CGU_LOG_HPP
#define CGU_LOG_HPP

#ifndef CGU_LOG_MACROS_HPP
#define CGU_LOG_MACROS_HPP
#include "log_macros_base.hpp"
#undef CGU_LOG_MACROS_HPP
#endif

#include "scope.hpp"
#include "ring_buffer.hpp"

#include <cstdio>
#include <cassert>

#include <string>
#include <string_view>
#include <chrono>
#include <utility>
#include <tuple>
#include <type_traits>
#include <atomic>
#include <vector>
#include <mutex>
#include <shared_mutex>
#include <thread>
#include <unordered_map>
#include <filesystem>
#include <functional>

#include <fmt/format.h>
#include <fmt/color.h>

#ifndef CGU_NO_IMGUI
#include <imgui.h>
#else
struct ImVec4 {
	float x, y, z, w;
};
#endif

namespace cgu {

	// forward declarations
	class log_source;
	class log_sink;

	enum class log_severity : unsigned char {
		info, warning, error
	};

	struct log_category {
		const static_scope_info *origin = nullptr;
		std::string_view name = "";
		int verbosity = 0;
		log_severity severity = log_severity::info;
		bool flush = false;
		bool transient = false;
	};

	// naughty class to (ab)use fmt's terminal styling support
	class fmt_style_helper {
	private:
		std::string m_style_off;
		std::unordered_map<std::string, std::string> m_styles;
		bool m_enabled = false;

	public:
		fmt_style_helper();

		void enable(bool b) noexcept {
			m_enabled = b;
		}

		void put(std::string_view name, const fmt::text_style &style);

		std::string_view get(std::string_view name) const;
	};

	struct log_message {
		const log_source *source = nullptr;
		log_category cat{};
		std::string_view file = "";
		int line = 0;
		std::chrono::system_clock::time_point time{};
		std::thread::id tid{};
		std::string message{};

		std::string format(std::string_view fmtstr, const fmt_style_helper &styles) const;

		uintptr_t proper_tid() const;
	};

	class log_submission {
	private:
		log_source *m_source = nullptr;
		log_category m_cat;
		fmt::memory_buffer m_msg;
		std::string_view m_file = "";
		int m_line = 0;

	public:
		~log_submission() {
			submit();
		}

		log_submission() = default;

		explicit log_submission(log_source &source_, const log_category &cat_, std::string_view file_, int line_) noexcept
			: m_source(&source_), m_cat(cat_), m_file(file_), m_line(line_)
		{}

		log_submission(const log_submission &) = default;
		log_submission & operator=(const log_submission &) = default;

		log_submission(log_submission &&other) noexcept :
			m_source(other.m_source),
			m_cat(other.m_cat),
			m_msg(std::move(other.m_msg)),
			m_file(other.m_file),
			m_line(other.m_line)
		{
			other.m_source = nullptr;
		}

		log_submission & operator=(log_submission &&other) noexcept {
			assert(!m_source && "cannot assign to in-progress log submission");
			m_source = std::exchange(other.m_source, nullptr);
			m_cat = other.m_cat;
			m_msg = std::move(other.m_msg);
			m_file = other.m_file;
			m_line = other.m_line;
			return *this;
		}

		log_submission & verbosity(int x) noexcept {
			m_cat.verbosity = x;
			return *this;
		}

		log_submission & flush(bool b = true) noexcept {
			m_cat.flush = b;
			return *this;
		}

		log_submission & transient(bool b = true) noexcept {
			m_cat.transient = b;
			return *this;
		}

		template <typename ...Ts>
		log_submission & operator()(std::string_view fmtstr, const Ts &...args) {
			fmt::format_to(m_msg, fmtstr, args...);
			return *this;
		}

		template <typename T, typename = std::void_t<decltype(std::declval<std::ostream &>() << std::declval<const T &>())>>
		log_submission & operator<<(const T &arg) {
			(*this)("{}", arg);
			return *this;
		}

		log_submission & append(std::string_view s) {
			m_msg.append(s);
			return *this;
		}

		void cancel() noexcept {
			m_source = nullptr;
			m_msg.clear();
		}

		void submit() noexcept;

	};

	namespace detail {
		template <typename ScopeTag, typename = void>
		struct scope_log_source;
	}

	// should generally be statically allocated (referred to by log_message objects).
	// const log_source is not writable.
	class log_source {
		template <typename, typename>
		friend struct detail::scope_log_source;
	private:
		struct sinklist_t {
			std::shared_mutex mutex;
			std::vector<log_sink *> sinks;
		};

		const static_scope_info *m_scope = nullptr;
		log_source *m_parent = nullptr;
		// lazy init; never deleted atm to avoid (static init) lifetime problems
		// TODO safely delete sinklist how?
		// TODO member storage?
		std::atomic<sinklist_t *> m_sinklist{nullptr};

	public:
		constexpr explicit log_source(const static_scope_info &scope_, log_source &parent_) :
			m_scope(&scope_), m_parent(&parent_) {}

		log_source(const log_source &) = delete;
		log_source & operator=(const log_source &) = delete;

		constexpr const static_scope_info * scope() const noexcept {
			return m_scope;
		}

		constexpr const log_source * parent() const noexcept {
			return m_parent;
		}

		constexpr log_source * parent() noexcept {
			return m_parent;
		}

		constexpr std::string_view name() const noexcept {
			return m_scope->name;
		}

		std::string fullname() const {
			return m_scope->fullname();
		}

		void submit(log_message &&) noexcept;

		void flush() noexcept;

		void attach(log_sink *);

		void detach(log_sink *) noexcept;

	private:
		struct global_root_source_tag{};
		constexpr explicit log_source(global_root_source_tag) : m_scope(&scope_global_root), m_parent(nullptr) {}
	};

	namespace detail {
		// msvc (16.10) blows up if we template by scope info pointer and try to access the parent.
		// workaround: template by scope tag type.
		template <typename ScopeTag, typename>
		struct scope_log_source {
			static inline log_source value{log_source::global_root_source_tag{}};
		};

CGU_WARNING_PUSH;
// gcc warns about ScopeTag::parent() not being nullable for non-root scope tags
CGU_PRAGMA_GCC_IGNORE(-Waddress);
		template <typename ScopeTag>
		struct scope_log_source<ScopeTag, std::enable_if_t<!!ScopeTag::parent()>> {
			constexpr static log_source &parent = scope_log_source<typename ScopeTag::parent_tag>::value;
			static inline log_source value{::cgu::detail::scope_by_tag<ScopeTag>, parent};
		};
CGU_WARNING_POP;
	}

	// global root log source
	constexpr inline log_source &log_global_root = detail::scope_log_source<scope_tag_global_root>::value;

	class log_sink {
	protected:
		mutable std::shared_mutex m_mutex;

	private:
		std::vector<log_source *> m_sources;
		int m_verbosity = 9001;

	protected:
		bool m_enable_transient = false;

	public:
		virtual ~log_sink();

		log_sink() = default;

		log_sink(const log_sink &) = delete;
		log_sink & operator=(const log_sink &) = delete;

		int verbosity() const noexcept {
			std::shared_lock lock(m_mutex);
			return m_verbosity;
		}

		void verbosity(int x) noexcept {
			std::unique_lock lock(m_mutex);
			m_verbosity = x;
		}

		void submit(const log_message &msg) noexcept {
			if (msg.cat.verbosity > m_verbosity) return;
			if (!m_enable_transient && msg.cat.transient) return;
			try {
				submit_impl(msg);
			} catch (...) {
				// TODO log sink failure handling
			}
		}

		virtual void flush() noexcept {}

		void notify_attach(log_source *);

		void notify_detach(log_source *) noexcept;

	private:
		virtual void submit_impl(const log_message &msg) = 0;
	};

	class text_log_sink : public log_sink {
	private:
		std::string m_fmtstr = default_format;
		fmt_style_helper m_styles;

	public:
		static constexpr const char *default_format = "{gmtime:%FT%T}.{millis:03}Z {cat:>8} [{filename}:{line}][{tid}][{source}] {msgbreak}{message}";

		text_log_sink() = default;

		text_log_sink(std::string fmtstr_) : m_fmtstr(std::move(fmtstr_)) {}

		std::string format_string() const {
			std::shared_lock lock(m_mutex);
			return m_fmtstr;
		}

		void format_string(std::string s) {
			std::unique_lock lock(m_mutex);
			m_fmtstr = s;
		}

		void put_style(const std::string &name, const fmt::text_style &style) {
			std::unique_lock lock(m_mutex);
			m_styles.put(name, style);
		}

		void enable_styles(bool b) {
			std::unique_lock lock(m_mutex);
			m_styles.enable(b);
		}

	protected:
		template <typename LockT>
		std::string format_message(LockT &lock, const log_message &msg) {
			assert(lock.owns_lock());
			return msg.format(m_fmtstr, m_styles);
		}
	};

	class console_log_sink : public text_log_sink {
	private:
		FILE *m_file = nullptr;
		std::string::size_type m_prev_transient_length = 0;
		bool m_is_tty = false;
		bool m_ansi = false;

	public:
		static constexpr const char *default_format =
			"{style:bold,black}{localtime:%T}.{millis:03}{style:off} "
			"{catstyle}{cat:>8}{style:off} [{tid:>5}][{style:bold,cyan}{source}{style:off}] "
			"{msgbreak}{message}";

		console_log_sink() : console_log_sink(stderr) {}
		explicit console_log_sink(FILE *);

		virtual void flush() noexcept override;

		bool ansi() const {
			return m_ansi;
		}

	private:
		virtual void submit_impl(const log_message &msg) override;
	};

	class file_log_sink : public text_log_sink {
	private:
		FILE *m_file = nullptr;

	public:
		virtual ~file_log_sink();

		explicit file_log_sink(const std::filesystem::path &, bool append_ = false);

		virtual void flush() noexcept override;

	private:
		virtual void submit_impl(const log_message &msg) override;
	};

#ifndef CGU_NO_IMGUI
	class imgui_log_sink : public log_sink {
	private:
		ring_buffer<log_message, ring_buffer_capacity_any> m_msgs;
		std::unordered_map<std::string, ImVec4> m_colors;
		float m_last_scroll = 0;
		bool m_autoscroll = true;

	public:
		imgui_log_sink();

		void put_color(const std::string &name, const ImVec4 &col);

		void draw();

	private:
		virtual void submit_impl(const log_message &) override;
		virtual void draw_message(const log_message &);
	};
#endif
	
}

template <>
struct fmt::formatter<cgu::fmt_style_helper> {
	std::vector<std::string> names;

	auto parse(format_parse_context& ctx) {
		auto it = ctx.begin();
		std::string name;
		for (; it != ctx.end() && *it != '}'; ++it) {
			if (*it == ',' && !name.empty()) {
				names.push_back(std::exchange(name, {}));
			} else {
				name += *it;
			}
		}
		if (!name.empty()) names.push_back(std::move(name));
		if (it != ctx.end() && *it != '}') throw format_error("invalid format");
		return it;
	}

	template <typename FormatContext>
	auto format(const cgu::fmt_style_helper &styles, FormatContext& ctx) const {
		auto out = ctx.out();
		for (auto &name : names) {
			out = format_to(out, styles.get(name));
		}
		return out;
	}
};

CGU_DEFINE_LOG_CATEGORY(cgu, debug, 5, info);
CGU_DEFINE_LOG_CATEGORY(cgu, trace, 4, info);
CGU_DEFINE_LOG_CATEGORY(cgu, timing, 4, info);
CGU_DEFINE_LOG_CATEGORY(cgu, locking, 4, info);
CGU_DEFINE_LOG_CATEGORY(cgu, status, 3, info);
CGU_DEFINE_LOG_CATEGORY(cgu, progress, 3, info, cat.transient=true);
CGU_DEFINE_LOG_CATEGORY(cgu, warning, 2, warning);
CGU_DEFINE_LOG_CATEGORY(cgu, error, 1, error, cat.flush=true);
CGU_DEFINE_LOG_CATEGORY(cgu, critical, 0, error, cat.flush=true);

#endif
