
/*
BSD 3-Clause License

Copyright (c) 2021, Benjamin Allen
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#pragma once

#ifndef CGU_MACROS_HPP
#define CGU_MACROS_HPP

#define CGU_STRINGIFY_IMPL(a) #a
#define CGU_STRINGIFY(a) CGU_STRINGIFY_IMPL(a)

#define CGU_TOKENPASTE_IMPL(a, b) a##b
#define CGU_TOKENPASTE(a, b) CGU_TOKENPASTE_IMPL(a, b)
#define CGU_TOKENPASTE3(a, b, c) CGU_TOKENPASTE(CGU_TOKENPASTE(a, b), c)
#define CGU_TOKENPASTE4(a, b, c, d) CGU_TOKENPASTE(CGU_TOKENPASTE3(a, b, c), d)

// putting this behind a macro helps intellisense track scope
#define CGU_NODISCARD [[nodiscard]]

#if defined(_MSC_VER)
// __pragma instead of _Pragma for intellisense's benefit
#define CGU_PRAGMA_GCC_IGNORE(w)
#define CGU_PRAGMA_MSVC_IGNORE(w) __pragma(warning(disable: w))
#define CGU_WARNING_PUSH __pragma(warning(push))
#define CGU_WARNING_POP __pragma(warning(pop))
#define CGU_WARNING_DISABLE_SHADOW CGU_PRAGMA_MSVC_IGNORE(4456 4459)
#elif defined(__GNUC__)
#define CGU_REQUIRE_SEMICOLON struct CGU_TOKENPASTE(_semicolon_, __COUNTER__){}
#define CGU_PRAGMA_GCC_IGNORE(w) _Pragma(CGU_STRINGIFY(GCC diagnostic ignored CGU_STRINGIFY(w))) CGU_REQUIRE_SEMICOLON
#define CGU_PRAGMA_MSVC_IGNORE(w)
#define CGU_WARNING_PUSH _Pragma("GCC diagnostic push") CGU_REQUIRE_SEMICOLON
#define CGU_WARNING_POP _Pragma("GCC diagnostic pop") CGU_REQUIRE_SEMICOLON
#define CGU_WARNING_DISABLE_SHADOW CGU_PRAGMA_GCC_IGNORE(-Wshadow)
#else
#define CGU_PRAGMA_GCC_IGNORE(w)
#define CGU_PRAGMA_MSVC_IGNORE(w)
#define CGU_WARNING_PUSH
#define CGU_WARNING_POP
#define CGU_WARNING_DISABLE_SHADOW
#endif

#if defined(_MSC_VER)
#define CGU_FUNC_STR __FUNCSIG__
#elif defined(__GNUC__)
#define CGU_FUNC_STR __PRETTY_FUNCTION__
#else
#define CGU_FUNC_STR __func__
#endif

#endif
