
/**
BSD 3-Clause License

Copyright (c) 2020, Benjamin Allen
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#pragma once

#ifndef CGU_PROFILER_MACROS_HPP
#define CGU_PROFILER_MACROS_HPP

#include "macros.hpp"
#include "scope_macros.hpp"

#include "profiler.hpp"

// needs scope info (see CGU_SCOPE_INFO).
// variadic args accept profile flags.
#define CGU_PROFILE_IMPL(defflags, ...) \
	CGU_WARNING_PUSH; \
	CGU_WARNING_DISABLE_SHADOW; \
	static ::cgu::static_profile_opts _cgu_profile_opts \
	{&::cgu::detail::scope_by_tag<_cgu_scope_tag>, ::cgu::make_profile_flags<(defflags)>(__VA_ARGS__)}; \
	::cgu::profile_guard _cgu_profile_guard{&_cgu_profile_opts}; \
	CGU_WARNING_POP

#define CGU_PROFILE_CPU(...) CGU_PROFILE_IMPL(::cgu::profile_flags::cpu, __VA_ARGS__)
#define CGU_PROFILE_GPU(...) CGU_PROFILE_IMPL(::cgu::profile_flags::gpu, __VA_ARGS__)
#define CGU_PROFILE_CPU_GPU(...) CGU_PROFILE_IMPL(::cgu::profile_flags::cpu_gpu, __VA_ARGS__)

#endif
