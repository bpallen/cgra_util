
/*
BSD 3-Clause License

Copyright (c) 2020-2021, Benjamin Allen
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#pragma once

#ifndef CGU_SCOPE_HPP
#define CGU_SCOPE_HPP

#ifndef CGU_ROOT_SCOPE_NAME
// define this privately in each lib/exe
#define CGU_ROOT_SCOPE_NAME root
#endif

#include "macros.hpp"

#ifndef CGU_SCOPE_MACROS_HPP
#define CGU_SCOPE_MACROS_HPP
#include "scope_macros_base.hpp"
#undef CGU_SCOPE_MACROS_HPP
#endif

#include <cassert>
#include <string>
#include <string_view>
#include <array>

namespace cgu {

	// must be statically allocated.
	struct static_scope_info {
		// TODO std::source_location
		const static_scope_info *parent = nullptr;
		std::string_view name = "";
		std::string_view func = "";
		std::string_view file = "";
		int line = 0;

		static_scope_info(const static_scope_info &) = delete;
		static_scope_info & operator=(const static_scope_info &) = delete;

		template <typename ScopeTag>
		constexpr explicit static_scope_info(const ScopeTag &tag) :
			// TODO constraints
			// see CGU_DECLARE_SCOPE_TAG
			parent(tag.parent()), name(tag.name()), func(tag.func()), file(tag.file()), line(tag.line())
		{}

		std::string fullname() const {
			if (!parent) return std::string(name);
			auto s = parent->fullname();
			s += '.';
			s += name;
			return s;
		}
	};

	// tag struct for global scope root
	struct scope_tag_global_root {
		static constexpr const static_scope_info * parent() noexcept { return nullptr; }
		static constexpr std::string_view name() noexcept { return ""; }
		static constexpr std::string_view func() noexcept { return ""; };
		static constexpr std::string_view file() noexcept { return __FILE__; }
		static constexpr int line() noexcept { return __LINE__; }
	};

	namespace detail {
		// msvc (16.10) has problems with using the address of a static constexpr local as a
		// template param when the referenced object contains a pointer (to another constexpr object)
		// and template instantiation dereferences the pointer.
		// workaround: use a local tag type and a global variable template.
		template <typename ScopeTag>
		constexpr static_scope_info scope_by_tag{ScopeTag{}};
	}

	// global scope root
	constexpr inline const static_scope_info &scope_global_root = detail::scope_by_tag<scope_tag_global_root>;

}

// cgu library scope root
CGU_DEFINE_SCOPE_ROOT(cgu);

#endif
