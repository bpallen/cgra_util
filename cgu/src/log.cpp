
/*
BSD 3-Clause License

Copyright (c) 2021, Benjamin Allen
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <cgu/log_macros.hpp>

#include <cgu/log.hpp>

#include <fmt/chrono.h>

#ifndef CGU_NO_IMGUI
#include <imgui.h>
#endif

#include <cstring>
#include <cstdint>
#include <algorithm>

#ifdef _WIN32
#include <io.h>
#define ENABLE_VIRTUAL_TERMINAL_PROCESSING 0x0004
extern "C" {
	__declspec(dllimport) bool __stdcall SetConsoleMode(void *hConsole, int dwMode);
	__declspec(dllimport) bool __stdcall GetConsoleMode(void *hConsole, int *lpMode);
}
#endif

CGU_SCOPE_INFO_NAMESPACE_PARENT(log, cgu);

namespace cgu {

	std::string log_message::format(std::string_view fmtstr, const fmt_style_helper &styles) const {
		using namespace std::chrono;
		const auto secs = floor<seconds>(time);
		const auto millis = (time - secs) / milliseconds(1);
		const auto legacy_time = system_clock::to_time_t(decltype(time)(secs));
		std::string_view filename = [&]{
#ifdef _WIN32
			auto i = file.find_last_of("\\/");
#else
			auto i = file.rfind('/');
#endif
			return i != std::string::npos ? file.substr(i + 1) : file;
		}();
		return fmt::format(
			fmtstr,
			fmt::arg("source", source->fullname()),
			fmt::arg("filename", filename),
			fmt::arg("line", line),
			fmt::arg("localtime", fmt::localtime(legacy_time)),
			fmt::arg("gmtime", fmt::gmtime(legacy_time)),
			fmt::arg("millis", millis),
			fmt::arg("tid", proper_tid()),
			fmt::arg("cat", cat.name),
			fmt::arg("msgbreak", message.find_first_of('\n') != std::string::npos ? "\n" : ""),
			fmt::arg("message", message),
			fmt::arg("style", styles),
			fmt::arg("catstyle", styles.get(cat.name))
		);
	}

	uintptr_t log_message::proper_tid() const {
		// std::thread::id is silly, so we have to be naughty to get an integer thread id
		uintptr_t itid = 0;
		if constexpr (sizeof(tid) == sizeof(short)) {
			unsigned short x = 0;
			std::memcpy(&x, &tid, sizeof(x));
			itid = x;
		} else if constexpr (sizeof(tid) == sizeof(int)) {
			unsigned int x = 0;
			std::memcpy(&x, &tid, sizeof(x));
			itid = x;
		} else if constexpr (sizeof(tid) == sizeof(long long)) {
			unsigned long long x = 0;
			std::memcpy(&x, &tid, sizeof(x));
			itid = x;
		}
		return itid;
	}

	void log_submission::submit() noexcept {
		if (m_source) {
			log_message msg{m_source, m_cat, m_file, m_line, std::chrono::system_clock::now(), std::this_thread::get_id()};
			msg.message = to_string(m_msg);
			m_source->submit(std::move(msg));
			if (m_cat.flush) m_source->flush();
			m_source = nullptr;
		}
	}

	void log_source::submit(log_message &&msg) noexcept {
		auto *sinklist = m_sinklist.load(std::memory_order_acquire);
		if (sinklist) {
			std::shared_lock lock(sinklist->mutex);
			for (auto *sink : sinklist->sinks) sink->submit(msg);
		}
		if (m_parent) m_parent->submit(std::move(msg));
	}

	void log_source::attach(log_sink *newsink) {
		// TODO test
		if (!newsink) return;
		auto *sinklist = m_sinklist.load(std::memory_order_acquire);
		if (!sinklist) {
			auto *newsinklist = new sinklist_t{};
			if (m_sinklist.compare_exchange_strong(sinklist, newsinklist, std::memory_order_release, std::memory_order_relaxed)) {
				sinklist = newsinklist;
			} else {
				// compare-exchange failed => sinklist already created
				// sinklist now contains loaded value
				delete newsinklist;
			}
		}
		{
			std::unique_lock lock(sinklist->mutex);
			auto &sinks = sinklist->sinks;
			auto it = std::find(sinks.begin(), sinks.end(), newsink);
			if (it == sinks.end()) {
				sinks.push_back(newsink);
				lock.unlock();
				newsink->notify_attach(this);
			}
		}
	}

	void log_source::detach(log_sink *oldsink) noexcept {
		// TODO test
		// must be noexcept for dtor safety
		if (!oldsink) return;
		auto *sinklist = m_sinklist.load(std::memory_order_acquire);
		if (!sinklist) return;
		{
			std::unique_lock lock(sinklist->mutex);
			auto &sinks = sinklist->sinks;
			auto it = std::find(sinks.begin(), sinks.end(), oldsink);
			if (it != sinks.end()) {
				// no allocation should happen here so noexcept is ok
				sinks.erase(it);
				lock.unlock();
				oldsink->notify_detach(this);
			}
		}
	}

	void log_source::flush() noexcept {
		auto *sinklist = m_sinklist.load(std::memory_order_acquire);
		if (sinklist) {
			std::shared_lock lock(sinklist->mutex);
			for (auto *sink : sinklist->sinks) sink->flush();
		}
		if (m_parent) m_parent->flush();
	}

	log_sink::~log_sink() {
		auto sources = [&]() {
			std::unique_lock lock(m_mutex);
			return std::exchange(m_sources, {});
		}();
		for (auto *log : sources) {
			log->detach(this);
		}
	}

	void log_sink::notify_attach(log_source *log) {
		std::unique_lock lock(m_mutex);
		m_sources.push_back(log);
	}

	void log_sink::notify_detach(log_source *log) noexcept {
		std::unique_lock lock(m_mutex);
		// noexcept should be ok because no allocation should happen
		m_sources.erase(std::remove(m_sources.begin(), m_sources.end(), log), m_sources.end());
	}

	fmt_style_helper::fmt_style_helper() {
		fmt::memory_buffer reset_buf;
		fmt::detail::reset_color(reset_buf);
		m_style_off = to_string(reset_buf);
		m_styles["off"] = m_style_off;
		put("bold", fmt::emphasis::bold);
		put("black", fmt::fg(fmt::terminal_color::black));
		put("red", fmt::fg(fmt::terminal_color::red));
		put("green", fmt::fg(fmt::terminal_color::green));
		put("yellow", fmt::fg(fmt::terminal_color::yellow));
		put("blue", fmt::fg(fmt::terminal_color::blue));
		put("magenta", fmt::fg(fmt::terminal_color::magenta));
		put("cyan", fmt::fg(fmt::terminal_color::cyan));
		put("white", fmt::fg(fmt::terminal_color::white));
	}

	void fmt_style_helper::put(std::string_view name, const fmt::text_style &style) {
		fmt::memory_buffer buf;
		if (style.has_emphasis()) {
			auto emphasis = fmt::detail::make_emphasis<char>(style.get_emphasis());
			buf.append(emphasis.begin(), emphasis.end());
		}
		if (style.has_foreground()) {
			auto foreground = fmt::detail::make_foreground_color<char>(style.get_foreground());
			buf.append(foreground.begin(), foreground.end());
		}
		if (style.has_background()) {
			auto background = fmt::detail::make_background_color<char>(style.get_background());
			buf.append(background.begin(), background.end());
		}
		m_styles[std::string(name)] = to_string(buf);
	}

	std::string_view fmt_style_helper::get(std::string_view name) const {
		if (!m_enabled) return "";
		auto it = m_styles.find(std::string(name));
		if (it != m_styles.end()) return it->second;
		return m_style_off;
	}

	console_log_sink::console_log_sink(FILE *file_) : text_log_sink(default_format), m_file{file_} {
		if (!m_file) return;
		put_style("debug", fmt::fg(fmt::terminal_color::bright_magenta));
		put_style("trace", fmt::fg(fmt::terminal_color::bright_blue));
		put_style("timing", fmt::fg(fmt::terminal_color::green));
		put_style("locking", fmt::fg(fmt::terminal_color::green));
		put_style("status", fmt::fg(fmt::terminal_color::bright_green));
		put_style("progress", fmt::fg(fmt::terminal_color::bright_green));
		put_style("warning", fmt::fg(fmt::terminal_color::yellow));
		put_style("error", fmt::fg(fmt::terminal_color::bright_red));
		put_style("critical", fmt::fg(fmt::terminal_color::red));
#ifdef _WIN32
		void *h = (void *) _get_osfhandle(_fileno(file_));
		int mode = 0;
		if (GetConsoleMode(h, &mode)) {
			// is a console
			m_is_tty = true;
			m_enable_transient = true;
			if (SetConsoleMode(h, mode | ENABLE_VIRTUAL_TERMINAL_PROCESSING)) {
				// ansi escape codes enabled (color etc)
				m_ansi = true;
			}
		}
#endif
		// TODO linux tty

		enable_styles(m_ansi);
	}

	void console_log_sink::submit_impl(const log_message &msg) {
		if (!m_file) return;
		// unique lock to deal with transients correctly
		std::unique_lock lock(m_mutex);
		auto str = format_message(lock, msg);
		// can't handle transient multiline messages
		if (msg.cat.transient && str.find('\n') != std::string::npos) return;
		// maybe overwrite previous transient message
		if (m_prev_transient_length) fputc(msg.cat.transient ? '\r' : '\n', m_file);
		const auto transient_length = msg.cat.transient ? str.length() : 0;
		// pad to previous transient length
		if (msg.cat.transient) while (str.length() < m_prev_transient_length) str += ' ';
		m_prev_transient_length = transient_length;
		if (!msg.cat.transient) str += "\n";
		std::fputs(str.c_str(), m_file);
	}

	void console_log_sink::flush() noexcept {
		if (!m_file) return;
		std::fflush(m_file);
	}

	file_log_sink::file_log_sink(const std::filesystem::path &fpath_, bool append_) : text_log_sink() {
		auto fpath = absolute(fpath_);
#ifdef _WIN32
		m_file = _wfopen(fpath.c_str(), append_ ? L"ab" : L"wb");
#else
		m_file = fopen(fpath.c_str(), append_ ? "ab" : "wb");
#endif
		if (m_file) {
			CGU_LOG(status)("opened log file \"{}\"", fpath.u8string()).verbosity(1);
		} else {
			CGU_LOG(error)("failed to open log file \"{}\"", fpath.u8string());
		}
	}

	file_log_sink::~file_log_sink() {
		if (m_file) std::fclose(m_file);
	}

	void file_log_sink::submit_impl(const log_message &msg) {
		if (!m_file) return;
		std::shared_lock lock(m_mutex);
		auto str = format_message(lock, msg);
		str += "\n";
		std::fputs(str.c_str(), m_file);
	}

	void file_log_sink::flush() noexcept {
		if (!m_file) return;
		std::fflush(m_file);
	}

#ifndef CGU_NO_IMGUI
	imgui_log_sink::imgui_log_sink() : m_msgs(1024) {
		m_enable_transient = true;
		put_color("debug", {0.7f, 0, 0.7f, 1});
		put_color("trace", {0.4f, 0.4f, 1, 1});
		put_color("timing", {0.1f, 0.6f, 0.1f, 1});
		put_color("locking", {0.1f, 0.6f, 0.1f, 1});
		put_color("status", {0, 0.9f, 0, 1});
		put_color("progress", {0, 0.9f, 0, 1});
		put_color("warning", {0.8f, 0.6f, 0, 1});
		put_color("error", {1, 0.2f, 0.2f, 1});
		put_color("critical", {1, 0, 0, 1});
	}

	void imgui_log_sink::draw_message(const log_message &msg) {
		using namespace ImGui;
		auto FmtTextColored = [](const ImVec4 &col, std::string_view fmtstr, const auto &...args) {
			PushStyleColor(ImGuiCol_Text, col);
			fmt::memory_buffer buf;
			fmt::vformat_to(buf, fmtstr, fmt::make_format_args(args...));
			TextUnformatted(buf.begin(), buf.end());
			PopStyleColor();
		};
		Selectable("", false, ImGuiSelectableFlags_DontClosePopups);
		const bool hover = IsItemHovered();
		ImVec4 levelcol{1, 1, 1, 1};
		if (auto it = m_colors.find(std::string(msg.cat.name)); it != m_colors.end()) levelcol = it->second;
		if (hover) {
			BeginTooltip();
			FmtTextColored({0.7f, 0.7f, 0.7f, 1}, "{}", msg.format("{gmtime:%FT%T}.{millis:03}Z", fmt_style_helper()));
			SameLine();
			FmtTextColored(levelcol, "{}", msg.cat.name);
			SameLine();
			FmtTextColored({0, 0.8f, 0.8f, 1}, "{}", msg.source->fullname());
			FmtTextColored({1, 1, 0, 1}, "{}:{}", msg.file, msg.line);
			FmtTextColored({0.7f, 0.7f, 0.7f, 1}, "{}", msg.source->scope()->func);
			Separator();
			PushTextWrapPos(600);
			TextUnformatted(msg.message.c_str());
			PopTextWrapPos();
			EndTooltip();
		}
		SameLine();
		FmtTextColored({0.7f, 0.7f, 0.7f, 1}, "{}", msg.format("{localtime:%T}.{millis:03}", fmt_style_helper()));
		SameLine();
		FmtTextColored(hover ? ImVec4{1, 1, 1, 1} : levelcol, "{:>8}", msg.cat.name);
		SameLine();
		FmtTextColored({0.7f, 0.7f, 0.7f, 1}, "{}", msg.proper_tid());
		SameLine();
		FmtTextColored({0, 0.8f, 0.8f, 1}, "{}", msg.source->fullname());
		SameLine();
		FmtTextColored({1, 1, 0, 1}, ">");
		// can't show multiline messages in list (height must be consistent)
		auto mstr = std::string_view(msg.message).substr(0, msg.message.find_first_of("\r\n"));
		SameLine();
		TextUnformatted(mstr.data(), mstr.data() + mstr.length());
	}

	void imgui_log_sink::put_color(const std::string &name, const ImVec4 &col) {
		std::unique_lock lock(m_mutex);
		m_colors[name] = col;
	}

	void imgui_log_sink::draw() {
		using namespace ImGui;
		std::shared_lock lock(m_mutex);
		if (BeginChild("logger")) {			
			ImGuiListClipper clipper(int(m_msgs.size()));
			while (clipper.Step()) {
				for (auto i = clipper.DisplayStart; i < clipper.DisplayEnd; i++) {
					draw_message(m_msgs[i]);
				}
			}
			// autoscroll
			const float scrolly = GetScrollY();
			const bool autoscroll = m_autoscroll && scrolly >= m_last_scroll;
			m_last_scroll = scrolly;
			m_autoscroll = scrolly == GetScrollMaxY();
			if (autoscroll) SetScrollHereY();
		}
		EndChild();
	}

	void imgui_log_sink::submit_impl(const log_message &msg) {
		std::unique_lock lock(m_mutex);
		if (msg.cat.transient && m_msgs.size() && m_msgs.back().cat.transient) {
			// overwrite previous transient message
			m_msgs.back() = msg;
		} else {
			m_msgs.push_back(msg);
		}
	}
#endif
	
}
