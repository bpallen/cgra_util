
/**
BSD 3-Clause License

Copyright (c) 2020, Benjamin Allen
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <cgu/log_macros.hpp>
#include <cgu/profiler_macros.hpp>

#ifndef CGU_NO_IMGUI
#include <imgui.h>
#endif

#include <cstdio>
#include <algorithm>
#include <iostream>

CGU_SCOPE_INFO_NAMESPACE_PARENT(profiler, cgu);

namespace frameroot {
	CGU_SCOPE_INFO_NAMESPACE(frameroot);
	namespace framebind {
		CGU_SCOPE_INFO_NAMESPACE_PARENT(framebind, frameroot);
	}
}

namespace {
	cgu::static_profile_opts root_section{};
	constexpr cgu::detail::profile_guard_data root_section_guard{nullptr, &root_section};
	thread_local const cgu::detail::profile_guard_data *current_section_guard = &root_section_guard;
	thread_local cgu::profile_frame *current_frame = nullptr;

	using frameroot::CGU_SCOPE_TAG(frameroot);
	using frameroot::framebind::CGU_SCOPE_TAG(framebind);

	// special section for the root of a frame
	// this section does not really get entered and cannot (properly) collect profile timing
	cgu::static_profile_opts section_frame_root{&CGU_SCOPE_BY_TAG(frameroot), cgu::profile_flags::none};

	// special section for a frame binding
	// this section does not really get entered (but does collect profile timing)
	cgu::static_profile_opts section_frame_bind{&CGU_SCOPE_BY_TAG(framebind), cgu::profile_flags::cpu_gpu};
}

namespace cgu {

	namespace detail {

		void section_profile_enter(profile_guard_data *g) {
			// TODO micro optimize?
			g->m_frame = current_frame;
			// current_section (and therefore parent) is never null
			const bool enable_profile = g->m_parent->m_enable_profile_children;
			g->m_enable_profile_children = enable_profile && !g->m_section->test(profile_flags::disable);
			// frame parent is the real parent only if in the same frame
			const size_t frame_parent = (g->m_frame == g->m_parent->m_frame) ? g->m_parent->m_frame_index : g->m_frame->_bind_index();
			if (!!g->m_frame & enable_profile) {
				g->m_frame_index = g->m_frame->_enter(frame_parent, g->m_section);
			} else {
				// ensure exit matches
				g->m_frame = nullptr;
			}
		}

		void section_profile_exit(profile_guard_data *g) noexcept {
			if (g->m_frame) g->m_frame->_exit(g->m_frame_index);
		}

	}

	profile_guard::~profile_guard() {
		assert(current_section_guard == this);
		detail::section_profile_exit(this);
		current_section_guard = parent();
	}

	profile_guard::profile_guard(static_profile_opts *section) noexcept {
		assert(section);
		m_section = section;
		m_parent = current_section_guard;
		current_section_guard = this;
		detail::section_profile_enter(this);
	}

	const profile_guard * profile_guard::current() noexcept {
		return static_cast<const profile_guard *>(current_section_guard);
	}

	const profile_guard * profile_guard::root() noexcept {
		return static_cast<const profile_guard *>(&root_section_guard);
	}

	profile_frame::profile_frame(profiler *prof, const profile_task *task) : m_prof(prof), m_task(task) {
		// TODO reserve space for stats?
		m_stats.push_back({&section_frame_root});
	}

	void profile_frame::bind(profile_flags enable, profiler_gpu_resources *gpures) {
		assert(m_prof && "cannot bind profile frame without profiler");
		assert(!bound() && "cannot bind already-bound profile frame");
		assert(!m_closed && "cannot bind profile frame after closure");
		assert((!(enable & profile_flags::gpu) || gpures) && "gpu profiling requires gpu resources instance");
		m_current_parent = current_frame;
		current_frame = this;
		m_current_gpures = gpures;
		m_current_tid = std::this_thread::get_id();
		m_current_enable = enable;
		m_current_bind_index = _enter(0, &section_frame_bind);
		m_current_bind_depth = m_current_parent ? m_current_parent->m_current_bind_depth + 1 : 0;
		m_prof->_bind(this);
	}

	void profile_frame::unbind() noexcept {
		assert(current_frame == this);
		assert(m_current_tid == std::this_thread::get_id());
		_exit(m_current_bind_index);
		m_prof->_unbind(this);
		current_frame = m_current_parent;
		m_current_parent = nullptr;
		m_current_gpures = nullptr;
		m_current_tid = {};
		m_current_enable = {};
		m_current_bind_index = 0;
	}

	profile_frame_stats profile_frame::combined_stats() const noexcept {
		assert(m_ready);
		profile_frame_stats fs;
		for (size_t i = 1; i < m_stats.size(); i += size_t(m_stats[i].descendants) + 1) {
			auto &bs = m_stats[i];
			if (!!(bs.flags & profile_flags::cpu)) {
				if (!(fs.flags & profile_flags::cpu)) {
					fs.time_cpu_enter = bs.time_cpu_enter;
					fs.time_cpu_exit = bs.time_cpu_exit;
				}
				fs.duration_cpu += bs.time_cpu_exit - bs.time_cpu_enter;
				fs.time_cpu_exit = std::max(fs.time_cpu_exit, bs.time_cpu_exit);
			}
			if (!!(bs.flags & profile_flags::gpu)) {
				if (!(fs.flags & profile_flags::gpu)) {
					fs.time_gpu_enter = bs.time_gpu_enter;
					fs.time_gpu_exit = bs.time_gpu_exit;
				}
				fs.duration_gpu += bs.time_gpu_exit - bs.time_gpu_enter;
				fs.time_gpu_exit = std::max(fs.time_gpu_exit, bs.time_gpu_exit);
			}
			fs.flags = fs.flags | bs.flags;
		}
		return fs;
	}

	size_t profile_frame::_enter(size_t parent, static_profile_opts *section) noexcept {
		assert(m_stats.size() > parent);
		size_t index = m_stats.size();
		m_stats.push_back({section, parent});
		auto &s = m_stats.back();
		// note - parent should always exist because of the special frame root section
		auto &p = m_stats[parent];
		s.tid = m_current_tid;
		s.depth = p.depth + 1;
		enter_timestamp(index);
		//std::cout << "entering section index=" << index << " parent=" << parent << " depth=" << s.depth << " " << section->name << std::endl;
		return index;
	}

	void profile_frame::_exit(size_t index) noexcept {
		auto &s = m_stats[index];
		auto &p = m_stats[s.parent];
		static_profile_opts *section = s.section;
		p.descendants += 1 + s.descendants;
		exit_timestamp(index);
		//std::cout << "exiting section time=" << ((s.time_cpu_exit - s.time_cpu_enter) / std::chrono::microseconds(1)) << "us " << section->name << std::endl;
	}

	profile_frame * profile_frame::current() noexcept {
		return current_frame;
	}

	void profile_frame::enter_timestamp(size_t index) noexcept {
		auto &s = m_stats[index];
		s.flags = m_current_enable & s.section->flags;
		if (!!(s.flags & profile_flags::cpu)) s.time_cpu_enter = std::chrono::steady_clock::now();
		if (!!(s.flags & profile_flags::gpu)) m_current_gpures->_enter(s);
	}

	void profile_frame::exit_timestamp(size_t index) noexcept {
		auto &s = m_stats[index];
		if (!!(s.flags & profile_flags::cpu)) s.time_cpu_exit = std::chrono::steady_clock::now();
		if (!!(s.flags & profile_flags::gpu)) m_current_gpures->_exit(s);
	}

	profiler::profiler() {
		m_frames.reserve(max_frames);
		m_gpuqueries.reserve(max_frames);
	}

	profile_frame * profiler::new_frame(const profile_task *task) {
		std::lock_guard lock(m_mtx);
		if (m_frames.size() >= max_frames) return nullptr;
		auto frame = std::make_unique<profile_frame>(this, task);
		profile_frame *p = frame.get();
		m_frames.push_back(std::move(frame));
		return p;
	}

	void profiler::end_frame(profile_frame *frame) noexcept {
		assert(frame);
		assert(!frame->bound() && "can't end currently bound profile frame");
		assert(!frame->m_closed && "profile frame is already closed");
		assert((std::find_if(m_frames.begin(), m_frames.end(), [&](auto &f) { return f.get() == frame; }) != m_frames.end()) && "profile frame not owned by this profiler");
		// mutex protects frame state too
		std::unique_lock lock(m_mtx);
		frame->m_closed = true;
		// tick here because only now is the frame actually closed and able to be made ready
		tick(nullptr, lock);
	}

	std::optional<profile_frame> profiler::poll() noexcept {
		std::unique_lock lock(m_mtx);
		for (auto it = m_frames.begin(); it != m_frames.end(); ++it) {
			auto &f = *it;
			if (f->m_ready) {
				auto ff = std::move(*f);
				m_frames.erase(it);
				return ff;
			}
		}
		return std::nullopt;
	}

	// called just after frame is bound
	void profiler::_bind(profile_frame *frame) {
		assert(frame);
		auto gpures = frame->m_current_gpures;
		if (gpures) {
			std::unique_lock lock(m_mtx);
			frame->m_clock_sync = gpures->clock_sync();
			// reserve vector capacity so unbind is 'actually' noexcept
			// note - all bound frames need a reserve
			m_gpuqueries.reserve(m_gpuqueries.size() + frame->m_current_bind_depth + 1);
			gpures->_bind(frame);
		}
	}

	// called just before frame is unbound
	void profiler::_unbind(profile_frame *frame) noexcept {
		assert(frame);
		auto gpures = frame->m_current_gpures;
		std::unique_lock lock(m_mtx);
		tick(gpures, lock);
		if (gpures) {
			profile_frame_gpu_queries queries{gpures, frame, frame->m_current_bind_index};
			// reserve in _bind so we know this doesnt allocate
			m_gpuqueries.push_back(std::move(queries));
			frame->m_outstanding_gpuqueries++;
			gpures->_unbind(frame);
		}
	}

	void profiler::tick(profiler_gpu_resources *gpures, const std::unique_lock<std::mutex> &lock) noexcept {
		assert(lock.owns_lock());
		// try make (previous) frames ready
		for (auto it =  m_gpuqueries.begin(); it != m_gpuqueries.end(); ) {
			auto &queries = *it;
			if (queries.gpures != gpures) {
				// can only process gpu queries for current gpu resources instance
				// (eg can't process queries for different context on different thread)
				++it;
				continue;
			}
			assert(gpures);
			auto &frame = *queries.frame;
			const size_t index = queries.index;
			const auto &s = frame.m_stats[index];
			if (gpures->_ready(s)) {
				// if the (exit) query for the frame binding is ready, we'll assume all queries under it are too
				for (size_t i = index; i < index + s.descendants + 1; i++) {
					gpures->_retrieve(frame.m_stats[i]);
				}
				frame.m_outstanding_gpuqueries--;
				it = m_gpuqueries.erase(it);
				// note - making ready here would not catch non-gpu frames
			} else {
				++it;
			}
		}
		for (auto &pframe : m_frames) {
			auto &frame = *pframe;
			if (frame.m_ready || !frame.m_closed) continue;
			if (frame.m_outstanding_gpuqueries <= 0) {
				// no remaining gpu queries or none to start with => ready
				frame.m_ready = true;
				if (auto cb = frame.m_task->callback) cb(frame.m_task, frame);
			}
		}
	}

	profiler_gpu_resources::~profiler_gpu_resources() {
		// note - any queries that are currently in use will be leaked
		// (we don't have a reliable way to reclaim them)
		glDeleteQueries(m_queries.size(), m_queries.data());
	}

	profiler_gpu_resources::profiler_gpu_resources() : m_queries(max_queries, 0) {
		// generate all query objects up front, and then never again to avoid allocation overhead
		glGenQueries(max_queries, m_queries.data());
		// initial clock sync
		update_clock_sync();
		m_next_present = m_clock_sync.first;
	}

	void profiler_gpu_resources::pre_present() noexcept {
		using namespace std;
		if (m_post_present_query) {
			CGU_SCOPE_INFO(present_sync);
			CGU_PROFILE_CPU();
			// wait for previous frame to complete
			// not using fence; relationship with timer queries seems unreliable
			GLint qa = false;
			glGetQueryObjectiv(m_post_present_query, GL_QUERY_RESULT_AVAILABLE, &qa);
			GLuint64 t = 0;
			// this will block until the result is available (ie previous frame is complete)
			glGetQueryObjectui64v(m_post_present_query, GL_QUERY_RESULT, &t);
			// if we waited for the timer query, set clock sync
			if (!qa) m_clock_sync = {chrono::steady_clock::now(), gpu_clock::time_point{} + chrono::nanoseconds{t}};
			free_query(m_post_present_query);
			m_post_present_query = 0;
		}
		m_pre_present_sync = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
		glFlush();
	}

	void profiler_gpu_resources::post_present() noexcept {
		m_post_present_query = alloc_query();
		if (!m_post_present_query) return;
		glQueryCounter(m_post_present_query, GL_TIMESTAMP);
		if (m_pre_present_sync) {
			switch (glClientWaitSync(m_pre_present_sync, 0, 0)) {
			case GL_ALREADY_SIGNALED:
			case GL_CONDITION_SATISFIED:
			{
				// this frame finished already, clock sync possible
				update_clock_sync();
				break;
			}
			case GL_TIMEOUT_EXPIRED:
				// not finished
				break;
			case GL_WAIT_FAILED:
			default:
				// broken
				assert(false && "glClientWaitSync failed");
				break;
			}
			glDeleteSync(m_pre_present_sync);
			m_pre_present_sync = nullptr;
		}
		// flush is taken care of by unbind
	}

	void profiler_gpu_resources::update_clock_sync() noexcept {
		CGU_SCOPE_INFO(update_clock_sync);
		GLuint q = alloc_query();
		if (!q) return;
		CGU_PROFILE_CPU();
		glQueryCounter(q, GL_TIMESTAMP);
		GLuint64 t = 0;
		// this will block until the query result is available
		glGetQueryObjectui64v(q, GL_QUERY_RESULT, &t);
		m_clock_sync = {std::chrono::steady_clock::now(), gpu_clock::time_point{} + std::chrono::nanoseconds{t}};
		free_query(q);
	}

	void profiler_gpu_resources::_enter(section_stats &stats) noexcept {
		if (m_queries.size() < 2) {
			// TODO out of query objects, flag this somehow?
			return;
		}
		// get query objects from pool
		stats.gpu_query_enter = alloc_query();
		stats.gpu_query_exit = alloc_query();
		// note - this records when previous commands are complete, not when the next one starts
		glQueryCounter(stats.gpu_query_enter, GL_TIMESTAMP);
	}

	void profiler_gpu_resources::_exit(section_stats &stats) noexcept {
		if (!stats.gpu_query_enter) {
			// didn't init queries
			return;
		}
		glQueryCounter(stats.gpu_query_exit, GL_TIMESTAMP);
	}

	bool profiler_gpu_resources::_ready(const section_stats &stats) noexcept {
		if (!stats.gpu_query_exit) return true;
		GLint status = 0;
		glGetQueryObjectiv(stats.gpu_query_exit, GL_QUERY_RESULT_AVAILABLE, &status);
		// if the exit query is ready, we'll assume the enter one is too
		return bool(status);
	}

	void profiler_gpu_resources::_retrieve(section_stats &stats) noexcept {
		if (!stats.gpu_query_exit) return;
		GLuint64 t0 = 0, t1 = 0;
		glGetQueryObjectui64v(stats.gpu_query_enter, GL_QUERY_RESULT, &t0);
		glGetQueryObjectui64v(stats.gpu_query_exit, GL_QUERY_RESULT, &t1);
		stats.time_gpu_enter = gpu_clock::time_point{} + std::chrono::nanoseconds{t0};
		stats.time_gpu_exit = gpu_clock::time_point{} + std::chrono::nanoseconds{t1};
		// return query objects to pool
		free_query(stats.gpu_query_enter);
		free_query(stats.gpu_query_exit);
		stats.gpu_query_enter = 0;
		stats.gpu_query_exit = 0;
	}

	void profiler_gpu_resources::_bind(profile_frame *frame) {

	}

	void profiler_gpu_resources::_unbind(profile_frame *frame) noexcept {
		// final flush ensures that gpu timing for e.g. present does not contain bubbles
		glFlush();
	}

}

#ifndef CGU_NO_IMGUI
namespace {
	struct frame_time_view {
		::std::chrono::steady_clock::time_point cpuorigin;
		::cgu::gpu_clock::time_point gpuorigin;
		::std::chrono::nanoseconds extent;
	};

	std::pair<double, const char *> preformat_duration(std::chrono::duration<double> dur) {
		static constexpr std::pair<double, const char *> units[]{
			{1e-9, "ns"},
			{1e-6, "us"},
			{1e-3, "ms"},
			{1, "s"},
			{60, "m"},
			{3600, "h"},
			{86400, "d"}
		};
		double a = std::abs(dur.count());
		auto *unit = &units[0];
		for (auto &u : units) {
			if (a >= u.first) {
				unit = &u;
			} else {
				break;
			}
		}
		return {dur.count() / unit->first, unit->second};
	}

	void draw_profile_frame_node(const ::cgu::profile_frame &frame, size_t node, const frame_time_view &timeview);

	void draw_profile_frame_children(const ::cgu::profile_frame &frame, size_t node, const frame_time_view &timeview) {
		using namespace ::ImGui;
		using namespace ::cgu;
		auto &stats = frame.stats();
		auto &s = stats[node];
		if (s.descendants > 0) {
			for (size_t j = node + 1; j < node + s.descendants + 1; j += size_t(stats[j].descendants) + 1) {
				static_profile_opts *sec = stats[j].section;
				// count how many times we've seen this section as a child for id purposes
				size_t c = 0;
				for (size_t k = node + 1; k < j; k += size_t(stats[k].descendants) + 1) {
					if (stats[k].section == sec) c++;
				}
				PushID(sec);
				PushID(c);
				draw_profile_frame_node(frame, j, timeview);
				PopID();
				PopID();
			}
		}
	}

	void draw_profile_frame_data(const ::cgu::profile_frame &frame, size_t node, const frame_time_view &timeview, bool rowhover) {
		using namespace ::ImGui;
		using namespace ::cgu;
		using namespace ::std;
		auto &stats = frame.stats();
		auto &s = stats[node];
		static_profile_opts *sec = s.section;
		const ImGuiCol col_cpu = 0xC000E8FF;
		const ImGuiCol col_gpu = 0xC01040FF;
		auto draw = GetWindowDrawList();
		const float h = GetTextLineHeight();
		const auto p00s = GetCursorScreenPos();
		const ImVec2 p0s{p00s.x - GetCursorPosX(), p00s.y - 1};
		ImVec2 p1s{p0s.x + GetWindowContentRegionWidth(), p0s.y + h + 2};
		if (rowhover) draw->AddRectFilled(p0s, p1s, 0x60FF8040);
		if (!!(s.flags & profile_flags::cpu)) {
			auto [v, u] = preformat_duration(s.time_cpu_exit - s.time_cpu_enter);
			TextColored(ColorConvertU32ToFloat4(col_cpu), "%#4.3g%s", v, u);
		}
		NextColumn();
		if (rowhover) draw->AddRectFilled(p0s, p1s, 0x60FF8040);
		if (!!(s.flags & profile_flags::gpu)) {
			auto [v, u] = preformat_duration(s.time_gpu_exit - s.time_gpu_enter);
			TextColored(ColorConvertU32ToFloat4(col_gpu), "%#4.3g%s", v, u);
		}
		NextColumn();
		{
			const float xmax = std::max(0.f, GetContentRegionAvail().x);
			const float barheight = floor(GetTextLineHeight() * 0.5f);
			const auto p0 = GetCursorScreenPos();
			if (rowhover) draw->AddRectFilled(p0s, p1s, 0x60FF8040);
			// note - drawlist works in real screen coords not local window coords
			if (!!(s.flags & profile_flags::cpu)) {
				const auto x0 = floor(std::clamp(xmax * (s.time_cpu_enter - timeview.cpuorigin) / timeview.extent, 0 * xmax, xmax));
				const auto x1 = floor(std::clamp(xmax * (s.time_cpu_exit - timeview.cpuorigin) / timeview.extent, 0 * xmax, xmax));
				if (x1 > 0 && x0 < xmax) {
					draw->AddRectFilled({p0.x + x0, p0.y}, {p0.x + x1 + 1, p0.y + barheight}, col_cpu);
					if (rowhover) {
						draw->AddLine({p0.x + x0, 0}, {p0.x + x0, 9001}, (col_cpu & 0x00FFFFFF) | 0x60000000);
						draw->AddLine({p0.x + x1, 0}, {p0.x + x1, 9001}, (col_cpu & 0x00FFFFFF) | 0x60000000);
					}
				}
			}
			if (!!(s.flags & profile_flags::gpu)) {
				const auto x0 = floor(std::clamp(xmax * (s.time_gpu_enter - timeview.gpuorigin) / timeview.extent, 0 * xmax, xmax));
				const auto x1 = floor(std::clamp(xmax * (s.time_gpu_exit - timeview.gpuorigin) / timeview.extent, 0 * xmax, xmax));
				if (x1 > 0 && x0 < xmax) {
					draw->AddRectFilled({p0.x + x0, p0.y + barheight}, {p0.x + x1 + 1, p0.y + barheight * 2}, col_gpu);
					if (rowhover) {
						draw->AddLine({p0.x + x0, 0}, {p0.x + x0, 9001}, (col_gpu & 0x00FFFFFF) | 0x60000000);
						draw->AddLine({p0.x + x1, 0}, {p0.x + x1, 9001}, (col_gpu & 0x00FFFFFF) | 0x60000000);
					}
				}
			}
		}
		NextColumn();
		// TODO tooltips?
	}

	void draw_profile_frame_node(const ::cgu::profile_frame &frame, size_t node, const frame_time_view &timeview) {
		using namespace ::ImGui;
		using namespace ::cgu;
		auto &stats = frame.stats();
		auto &s = stats[node];
		// FIXME storage ref is easily invalidated
		bool expanded = GetStateStorage()->GetBool(GetID("expanded"), false);
		const float h = GetTextLineHeight();
		const float x0 = GetCursorPosX();
		const auto p00s = GetCursorScreenPos();
		const ImVec2 p0s{p00s.x - x0, p00s.y - 1};
		auto *draw = GetWindowDrawList();
		ImVec2 p1s{p0s.x + GetWindowContentRegionWidth(), p0s.y + h + 2};
		const bool rowhover = IsMouseHoveringRect(p0s, p1s, false);
		if (rowhover) draw->AddRectFilled(p0s, p1s, 0x60FF8040);
		SetCursorPosX(x0 + (s.depth - 1) * 10);
		if (expanded) {
			if (InvisibleButton("v", {h, h})) GetStateStorage()->SetBool(GetID("expanded"), false);
		} else {
			if (InvisibleButton(">", {h, h})) GetStateStorage()->SetBool(GetID("expanded"), true);
		}
		SameLine();
		SetCursorPosX(x0);
		InvisibleButton("##fdsgh", {-1, h});
		if (IsMouseDoubleClicked(0) && IsItemHovered()) GetStateStorage()->SetBool(GetID("expanded"), !expanded);
		SameLine();
		SetCursorPosX(x0 + (s.depth - 1) * 10);
		if (expanded) {
			SmallButton("v");
		} else {
			SmallButton(">");
		}
		SameLine();
		Text("%s", std::string(s.section->scope->name).c_str());
		NextColumn();
		if (rowhover) draw->AddRectFilled(p0s, p1s, 0x60FF8040);
		PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2{0, 0});
		bool enable = !s.section->test(profile_flags::disable);
		Checkbox("##enable", &enable);
		s.section->set(profile_flags::disable, !enable);
		PopStyleVar();
		NextColumn();
		draw_profile_frame_data(frame, node, timeview, rowhover);
		if (expanded) {
			draw_profile_frame_children(frame, node, timeview);
		}
	}

	float choose_interval(float dur, float px, float tgtpx) {
		using namespace ::std;
		chrono::nanoseconds v{1}, vr{1};
		float bestc = 0;
		for (int div : {10, 5, 2}) {
			while (true) {
				float vf = float(v / 1.0s) * div;
				float c = dur / vf;
				if (c <= 1 || c * tgtpx <= px) {
					if (c > bestc) {
						vr = v * div;
						bestc = c;
					}
					break;
				}
				v *= div;
			}
		}
		return float(vr / 1.0s);
	}

}

namespace ImGui {

	void draw_profile_frame(const ::cgu::profile_frame &frame, ::std::chrono::nanoseconds expected_duration, float &scroll_origin, float &scroll_extent) {
		using namespace ::cgu;
		using namespace ::std;
		if (!frame.ready() || frame.stats().size() < 2) return;
		// should _not_ use frame specific id, so that ui state is persisted between frames
		PushID("profileframe");
		float &availw = *(GetStateStorage()->GetFloatRef(GetID("availw"), 0));
		bool winresize = availw != GetContentRegionAvail().x;
		// TODO new tables api to scroll frame contents with sticky header
		if (BeginChild("", {0, 0}, false, ImGuiWindowFlags_AlwaysVerticalScrollbar)) {
			auto clocksync = frame.clock_sync();
			profile_frame_stats fs = frame.combined_stats();
			frame_time_view timeview;
			timeview.cpuorigin = fs.time_cpu_enter + chrono::duration_cast<chrono::nanoseconds>(1.0s * scroll_origin);
			timeview.gpuorigin = clocksync.second + (timeview.cpuorigin - clocksync.first);
			timeview.extent = max(chrono::duration_cast<chrono::nanoseconds>(1.0s * scroll_extent), 1ns);
			auto draw = GetWindowDrawList();
			float &col0w = *(GetStateStorage()->GetFloatRef(GetID("col0w"), 180));
			Columns(5);
			if (winresize) SetColumnWidth(0, col0w);
			// TODO column padding control how?
			SetColumnWidth(1, 30);
			SetColumnWidth(2, 60);
			SetColumnWidth(3, 60);
			Text("Section");
			NextColumn();
			NextColumn();
			Text("CPU");
			NextColumn();
			Text("GPU");
			NextColumn();
			// note - drawlist works in real screen coords not local window coords
			const auto dragp0 = GetCursorScreenPos();
			const auto dragsize = GetContentRegionAvail();
			const float dragw = dragsize.x;
			// TODO how to make the whole timeline region draggable?
			Selectable("##timedrag", true);
			if (IsItemActive() && IsMouseDragging(0)) {
				float d = GetMouseDragDelta().x;
				ResetMouseDragDelta();
				scroll_origin = max(scroll_origin - (d / dragw) * scroll_extent, 0.f);
			}
			// TODO more compact times on timeline?
			const float interval = choose_interval(scroll_extent, dragw, 42);
			for (float t = ceil(scroll_origin / interval) * interval; t < scroll_origin + scroll_extent; t += interval) {
				float x = floor(dragp0.x + dragw * (t - scroll_origin) / scroll_extent);
				// TODO line length how?
				draw->AddLine({x, dragp0.y}, {x, dragp0.y + 9001}, 0x80888888);
				char buf[32];
				{
					auto [v, u] = preformat_duration(t * 1.0s);
					::snprintf(buf, sizeof(buf), "%.3g%s", v, u);
				}
				draw->AddText({x + 1, dragp0.y}, 0xE0FFFFFF, buf);
			}
			Separator();
			NextColumn();
			draw_profile_frame_children(frame, 0, timeview);
			col0w = GetColumnWidth(0);
		}
		EndChild();
		availw = GetContentRegionAvail().x;
		PopID();
	}

	void draw_profile_collector(::cgu::profile_collector &collector) {
		using namespace ::cgu;
		using namespace ::std;
		PushID("profilecollector");
		const char *taskpreview = collector.selected_task ? collector.selected_task->name.c_str() : "<none>";
		SetNextItemWidth(120);
		if (BeginCombo("Task", taskpreview)) {
			// TODO make this combo list sorted
			for (auto &p : collector.data) {
				if (!p.first) continue;
				if (Selectable(p.first->name.c_str(), p.first == collector.selected_task)) {
					collector.selected_task = p.first;
				}
			}
			EndCombo();
		}
		SameLine();
		if (!collector.data.empty()) {
			auto datait = collector.data.find(collector.selected_task);
			if (datait == collector.data.end()) {
				// current selected task not available
				datait = collector.data.begin();
				collector.selected_task = datait->first;
			}
			auto &task = datait->second;
			PushID(task.task);
			float &scrolltimeorigin = *GetStateStorage()->GetFloatRef(GetID("scrolltimeorigin"));
			float &scrolltimeextent = *GetStateStorage()->GetFloatRef(GetID("scrolltimeextent"), float(task.expected_duration / 1.0s));
			if (Button("Zoom-")) scrolltimeextent *= 1.25f;
			SameLine();
			if (Button("Zoom+")) scrolltimeextent /= 1.25f;
			SameLine();
			if (Button("Reset")) {
				scrolltimeorigin = 0;
				scrolltimeextent = float(task.expected_duration / 1.0s);
			}
			SameLine();
			if (Button(task.paused ? "|>" : "||")) {
				task.paused = !task.paused;
				if (!task.paused) task.selection = 0;
			}
			SameLine();
			if (Button("|<<")) task.selection++;
			SameLine();
			if (Button(">>|")) task.selection = max(task.selection - 1, 0);
			SameLine();
			if (Button("Sync")) collector.want_clock_sync = true;
			auto *frame = (size_t(task.selection) < task.frames.size()) ? &*(task.frames.rbegin() + task.selection) : nullptr;
			if (frame) {
				auto fstats = frame->combined_stats();
				if ((fstats.flags & profile_flags::cpu_gpu) == profile_flags::cpu_gpu) {
					const auto dsync = fstats.time_cpu_enter - frame->clock_sync().first;
					SameLine();
					if (abs(dsync) > 100ms) {
						TextColored({1.f, 0.3f, 0.1f, 1.f}, "Not Synced");
					} else {
						TextColored({0.f, 1.f, 0.3f, 1.f}, "Synced");
					}
				}
			}
			Separator();
			// TODO child window padding control how?
			if (BeginChild("frame-and-lagometer")) {
				auto avail = GetContentRegionAvail();
				Columns(2);
				if (IsWindowAppearing()) SetColumnWidth(0, avail.x * 0.8f);
				if (frame) {
					draw_profile_frame(*frame, task.expected_duration, scrolltimeorigin, scrolltimeextent);
				} else {
					TextDisabled("Invalid frame selected");
				}
				NextColumn();
				draw_profile_lagometer(task);
			}
			EndChild();
			PopID();
		}
		PopID();
	}

	void draw_profile_lagometer(::cgu::profile_collector::task_data &task) {
		using namespace ::cgu;
		using namespace ::std;
		const ImGuiCol linecol = 0xE0FFFFFF;
		// note - drawlist works in real screen coords not local window coords
		const auto p0 = GetCursorScreenPos();
		const auto avail = GetContentRegionAvail();
		const float hmax = max(0.f, floor(avail.y * 0.5f));
		auto draw = GetWindowDrawList();
		const auto gooddur = task.expected_duration;
		char gooddurstr[32]{};
		char baddurstr[32]{};
		{
			auto [gv, gu] = preformat_duration(gooddur);
			::snprintf(gooddurstr, sizeof(gooddurstr), "%.3g%s", gv, gu);
			auto [bv, bu] = preformat_duration(gooddur * 2);
			::snprintf(baddurstr, sizeof(baddurstr), "%.3g%s", bv, bu);
		}
		draw->AddLine({p0.x, p0.y + floor(hmax * 0.5f)}, {p0.x + avail.x, p0.y + floor(hmax * 0.5f)}, linecol);
		draw->AddLine({p0.x, p0.y + floor(hmax * 1.5f)}, {p0.x + avail.x, p0.y + floor(hmax * 1.5f)}, linecol);
		float barwidth = 1;
		float x = p0.x + avail.x;
		for (auto it = task.frames.rbegin(); x - barwidth >= p0.x && it != task.frames.rend(); ++it) {
			auto fs = it->combined_stats();
			if (it - task.frames.rbegin() == task.selection) {
				// show selected frame
				draw->AddRectFilled({x - barwidth, p0.y}, {x, p0.y + avail.y}, 0xB0FF8000);
			}
			if (!!(fs.flags & profile_flags::cpu)) {
				const float h = clamp(hmax * fs.duration_cpu / (gooddur * 2), 0.f, hmax);
				const float badness = clamp(1.f * (fs.duration_cpu - gooddur) / (gooddur), 0.f, 1.f);
				ImVec4 col{badness, 1.f - badness, 0, 0.7f};
				draw->AddRectFilled({x - barwidth, p0.y + hmax - h}, {x, p0.y + hmax}, ColorConvertFloat4ToU32(col));
			}
			if (!!(fs.flags & profile_flags::gpu)) {
				const float h = clamp(hmax * fs.duration_gpu / (gooddur * 2), 0.f, hmax);
				const float badness = clamp(1.f * (fs.duration_gpu - gooddur) / (gooddur), 0.f, 1.f);
				ImVec4 col{badness, 1.f - badness, 0, 0.7f};
				draw->AddRectFilled({x - barwidth, p0.y + hmax * 2 - h}, {x, p0.y + hmax * 2}, ColorConvertFloat4ToU32(col));
			}
			if (IsMouseHoveringRect({x - barwidth, p0.y}, {x, p0.y + avail.y})) {
				// hovered, maybe select
				ImGuiCol selcolor = 0x80FF8000;
				// TODO drag how?
				if (IsMouseDown(0)) {
					selcolor = 0xFFFF8000;
				}
				if (IsMouseClicked(0)) {
					task.selection = it - task.frames.rbegin();
				}
				// TODO tooltip with timing?
				draw->AddRectFilled({x - barwidth, p0.y}, {x, p0.y + avail.y}, selcolor);
			}
			x -= barwidth;
		}
		draw->AddText({p0.x, p0.y + 2}, linecol, baddurstr);
		draw->AddText({p0.x, p0.y + floor(hmax * 0.5f) + 2}, linecol, gooddurstr);
		draw->AddText({p0.x, p0.y + hmax + 2}, linecol, baddurstr);
		draw->AddText({p0.x, p0.y + floor(hmax * 1.5f) + 2}, linecol, gooddurstr);
		draw->AddLine({p0.x, p0.y}, {p0.x + avail.x, p0.y}, linecol);
		draw->AddLine({p0.x, p0.y + hmax - 1}, {p0.x + avail.x, p0.y + hmax - 1}, linecol);
		draw->AddLine({p0.x, p0.y + hmax * 2 - 1}, {p0.x + avail.x, p0.y + hmax * 2 - 1}, linecol);
		draw->AddText({p0.x + avail.x - 23, p0.y}, linecol, "CPU");
		draw->AddText({p0.x + avail.x - 23, p0.y + hmax}, linecol, "GPU");
	}

}
#endif

#ifdef _WIN32
// assume win7+
// at bottom of file to avoid name pollution
#define NOMINMAX
#pragma comment(lib, "dwmapi.lib")
#include <dwmapi.h>

namespace {
	std::chrono::steady_clock::time_point qpc_time_point(QPC_TIME _Ctr) noexcept {
		// implementation lifted from msvc steady_clock
		using namespace std::chrono;
		const long long _Freq = _Query_perf_frequency(); // doesn't change after system boot
		static_assert(steady_clock::period::num == 1, "This assumes period::num == 1.");
		const long long _Whole = (_Ctr / _Freq) * steady_clock::period::den;
		const long long _Part  = (_Ctr % _Freq) * steady_clock::period::den / _Freq;
		return steady_clock::time_point(steady_clock::duration(_Whole + _Part));
	}
}

namespace cgu {
	bool profiler_gpu_resources::try_wait_desktop_sync() noexcept {
		CGU_SCOPE_INFO(wait_desktop_sync);
		CGU_PROFILE_CPU();
		using namespace std;
		if (BOOL dwm = false; DwmIsCompositionEnabled(&dwm), !dwm) return false;
		DWM_TIMING_INFO timing;
		timing.cbSize = sizeof(DWM_TIMING_INFO);
		if (DwmGetCompositionTimingInfo(nullptr, &timing) != S_OK) return false;
		const auto vblank = qpc_time_point(timing.qpcVBlank);
		const auto compose_interval = timing.rateCompose.uiDenominator * chrono::duration_cast<chrono::nanoseconds>(1s) / timing.rateCompose.uiNumerator;
		this_thread::sleep_until(m_next_present);
		// try ensure the presented frame is composed on the desired vblank
		// rationale: we want to call present/swapbuffers just after vblank so that the gpu has
		// as near as possible an entire composition interval to finish the frame and actually present.
		// if the gpu present doesn't reliably fall between vblanks, we get stuttering.
		// TODO observe when gpu present occurs and adjust accordingly?
		auto target = vblank + 500us;
		const auto now = chrono::steady_clock::now();
		// the reported 'next' vblank can seemingly be out by a few frames in either direction
		while (target - now > compose_interval * 3 / 2) target -= compose_interval;
		while (target - now < compose_interval / 2) target += compose_interval;
		// disable sync if running behind (mimics nvidia adaptive vsync)
		if (target - now > compose_interval) target = now + compose_interval;
		m_next_present = target;
		// try clock sync now, to avoid waiting for gpu presentation in post_present()
		if (m_pre_present_sync) {
			switch (glClientWaitSync(m_pre_present_sync, 0, 0)) {
			case GL_ALREADY_SIGNALED:
			case GL_CONDITION_SATISFIED:
			{
				// this frame finished already, clock sync possible
				update_clock_sync();
				glDeleteSync(m_pre_present_sync);
				m_pre_present_sync = nullptr;
				break;
			}
			case GL_TIMEOUT_EXPIRED:
				// not finished
				break;
			case GL_WAIT_FAILED:
			default:
				// broken
				assert(false && "glClientWaitSync failed");
				break;
			}
		}
		return true;
	}
}

#else
namespace cgu {
	bool profiler_gpu_resources::try_wait_desktop_sync() noexcept {
		return false;
	}
}
#endif
