
# 2013-2021, Benjamin Allen
# 
# This is free and unencumbered software released into the public domain.
# 
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
# 
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
# 
# For more information, please refer to <http://unlicense.org/>

# require new behaviour of:
# CMP0054 (don't dereference quoted variables in if() args)
cmake_minimum_required(VERSION 3.1)

# Set CMake output dirs to "${CMAKE_BINARY_DIR}/bin" if the current project is the root project
function(cgu_maybe_set_output_dirs)
	# necessary for building shared libs so they all go in the same place and can then be loaded
	# however, the current value takes effect when a target is created, so overriding as a subproject would be bad
	# therefore, we only set the output directories if we are the top-level project
	# TODO other subprojects could still override this
	if("${CMAKE_SOURCE_DIR}" STREQUAL "${PROJECT_SOURCE_DIR}")
		message(STATUS "CGU: setting CMake output directories to '${CMAKE_BINARY_DIR}/bin'")
		set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin")
		set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin")
		set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin")
	endif()
endfunction()

# Set CGU default build options (warnings etc.)
function(cgu_target_default_build_options target)
	# TODO gcc/clang optimizaton flags?
	# TODO floating-point behaviour (msvc /fp:fast can break things)
	# TODO mixing signed/unsigned integers?
	if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC")
		# Multiprocess build
		target_compile_options(${target} PRIVATE /MP)
		# Disable non-standard behaviour
		target_compile_options(${target} PRIVATE /permissive-)
		# UTF-8 source and execution charsets
		target_compile_options(${target} PRIVATE /utf-8)
		# Full normal warnings
		target_compile_options(${target} PRIVATE /W4)
		# Disable C4800: forcing X to bool (performance warning)
		target_compile_options(${target} PRIVATE /wd4800)
		# Function-level linking
		target_compile_options(${target} PRIVATE /Gy)
	elseif("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
		# Full normal warnings
		target_compile_options(${target} PRIVATE -Wall -Wextra -pedantic)
		# Threading support
		target_compile_options(${target} PRIVATE -pthread)
		# Promote missing return to error
		target_compile_options(${target} PRIVATE -Werror=return-type)
	elseif("${CMAKE_CXX_COMPILER_ID}" MATCHES "^(Apple)?Clang$")
		# Full normal warnings
		target_compile_options(${target} PRIVATE -Wall -Wextra -pedantic)
		# Threading support
		target_compile_options(${target} PRIVATE -pthread)
		# Promote missing return to error
		target_compile_options(${target} PRIVATE -Werror=return-type)
	endif()
endfunction()

# Use fast linking of debug symbols (msvc /debug:fastlink)
function(cgu_target_debug_fastlink target)
	if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC")
		# always produce PDB
		target_compile_options(${target} PRIVATE /Zi)
		target_link_libraries(${target} PRIVATE "-debug:fastlink")
	endif()
endfunction()

# Build target with link-time codegen
function(cgu_target_ltcg target)
	if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC")
		# Link-time codegen
		target_link_libraries(${target} PRIVATE -ltcg:incremental)
		# Whole program optimization
		target_compile_options(${target} PRIVATE /GL)
		# LTCG also has to be specified separately for static libs
		set_property(TARGET ${target} APPEND PROPERTY STATIC_LIBRARY_FLAGS /ltcg)
	endif()
	# TODO GCC? Clang?
endfunction()

# Add MSVC-style compile options
function(cgu_target_msvc_compile_options)
	if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC")
		target_compile_options(${ARGV})
	endif()
endfunction()

# Add GCC-style compile options
function(cgu_target_gcc_compile_options)
	if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
		target_compile_options(${ARGV})
	elseif("${CMAKE_CXX_COMPILER_ID}" MATCHES "^(Apple)?Clang$")
		target_compile_options(${ARGV})
	endif()
endfunction()

# Set source groups to mirror real directory tree
# deprecated: use cmake's builtin functionality:
# source_group(TREE "${CMAKE_CURRENT_LIST_DIR}" FILES ${sources})
function(cgu_target_source_group_tree target)
	get_target_property(sources ${target} SOURCES)
	foreach(source ${sources})
		get_filename_component(source ${source} ABSOLUTE)
		string(REPLACE "${PROJECT_SOURCE_DIR}/" "" rel ${source})
		if(rel)
			string(REGEX REPLACE "/([^/]*)$" "" rel ${rel})
			if(NOT rel STREQUAL source)
				string(REPLACE "/" "\\\\" rel ${rel})
				source_group(${rel} FILES ${source})
			endif()
		endif()
	endforeach()
endfunction()

# Add sources to a target relative to current CMakeLists.txt directory (CMAKE_CURRENT_SOURCE_DIR)
function(cgu_target_relative_sources target)
	foreach(source ${ARGN})
		target_sources(${target} PRIVATE "${CMAKE_CURRENT_SOURCE_DIR}/${source}")
	endforeach()
endfunction()

# Create missing source files relative to current CMakeLists.txt directory (CMAKE_CURRENT_SOURCE_DIR)
function(cgu_create_missing)
	foreach(source ${ARGN})
		if(NOT IS_ABSOLUTE ${source})
			set(path "${CMAKE_CURRENT_SOURCE_DIR}/${source}")
			if (NOT EXISTS ${path})
				message(STATUS "cgu_create_missing: ${path}")
				file(APPEND ${path} "")
			endif()
		endif()
	endforeach()
endfunction()

# Embed a file as a raw string literal
function(cgu_target_embed_string target input name)
	get_filename_component(fname ${input} NAME)
	get_filename_component(fpath ${input} ABSOLUTE)
	set(fpath2 "${CMAKE_CURRENT_BINARY_DIR}/${fname}.hpp")
	set(gencmd "${CMAKE_CURRENT_BINARY_DIR}/${fname}.cmake")
	# probably dont need this: \\\"#line 1\\\\n\\\"
	# and it breaks things that aren't glsl, duh.
	file(WRITE ${gencmd} "
		file(READ \"${fpath}\" text)
		file(WRITE \"${fpath2}\" \"namespace cgu { namespace strings { static const char *${name} = R\\\"be3b8bb9686d4d32(\")
		file(APPEND \"${fpath2}\" \"\${text}\")
		file(APPEND \"${fpath2}\" \")be3b8bb9686d4d32\\\"; } }\")
	")
	add_custom_command(
		OUTPUT "${fpath2}"
		COMMAND ${CMAKE_COMMAND} -P ${gencmd}
		MAIN_DEPENDENCY "${fpath}"
		DEPENDS ${gencmd}
	)
	target_include_directories(${target} PRIVATE ${CMAKE_CURRENT_BINARY_DIR})
endfunction()
