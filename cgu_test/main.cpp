
/*
2013-2019, Benjamin Allen and Joshua Scott

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
*/

#include <cgu/log_macros.hpp>
#include <cgu/profiler_macros.hpp>

#include <cgu/opengl.hpp>
#include <cgu/util.hpp>
#include <cgu/mesh.hpp>
#include <cgu/shader.hpp>
#include <cgu/profiler.hpp>

#include <iostream>
#include <string>
#include <stdexcept>
#include <chrono>
#include <thread>
#include <typeinfo>

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

CGU_DEFINE_SCOPE_ROOT(test);

class log_scope_test {
private:
	CGU_EXTERN_SCOPE_INFO_CLASS(class_scope);
public:
	static void test();
};

CGU_SCOPE_INFO_NAMESPACE_PARENT(main, test);

using namespace std;

#ifdef _WIN32
// TODO why am i having to do this now? (windows 10 2004)
#pragma comment(lib, "winmm.lib")
extern "C" {
	__declspec(dllimport) int __stdcall timeBeginPeriod(unsigned uPeriod);
	__declspec(dllimport) int __stdcall timeEndPeriod(unsigned uPeriod);
}

class win32_time_period {
	unsigned m_period;
public:
	~win32_time_period() {
		timeEndPeriod(m_period);
	}

	win32_time_period(unsigned period) : m_period(period) {
		timeBeginPeriod(m_period);
	}

	win32_time_period(const win32_time_period &) = delete;
	win32_time_period & operator=(const win32_time_period &) = delete;
};
#endif

namespace {
	void cursor_pos_callback(GLFWwindow *, double xpos, double ypos);
	void mouse_button_callback(GLFWwindow *win, int button, int action, int mods);
	void scroll_callback(GLFWwindow *win, double xoffset, double yoffset);
	void key_callback(GLFWwindow *win, int key, int scancode, int action, int mods);
	void char_callback(GLFWwindow *win, unsigned int c);
	void APIENTRY gl_debug_callback(GLenum, GLenum, GLuint, GLenum, GLsizei, const GLchar *, const GLvoid *);

	ImGuiContext *imguictx;

	float zfar = 1000;
	cgu::orbital_camera cam;
	
	cgu::gl_framebuffer fb_scene{
		cgu::gl_rendertarget_params{GL_DEPTH_ATTACHMENT, {GL_TEXTURE_2D, GL_DEPTH_COMPONENT24, cgu::gl_texture_mipmaps::none, GL_DEPTH_COMPONENT, GL_FLOAT}},
		cgu::gl_rendertarget_params{GL_COLOR_ATTACHMENT0, {GL_TEXTURE_2D, GL_RGBA8, cgu::gl_texture_mipmaps::none}}
	};
}

int main() {

	CGU_SCOPE_INFO(main);

	cgu::console_log_sink conlog;
	//conlog.verbosity(cgu::log_level::warning.verbosity);
	cgu::log_global_root.attach(&conlog);

	cgu::file_log_sink filelog{"test.log", false};
	cgu::log_global_root.attach(&filelog);

	cgu::imgui_log_sink imguilog{};
	cgu::log_global_root.attach(&imguilog);

#ifdef _WIN32
	win32_time_period _timeperiod(1);
#endif

	CGU_LOG(debug)("ansi? {}", conlog.ansi());
	CGU_LOG(status)("starting in {}", 123).verbosity(0);
	CGU_LOG(error)("woops") << " haha " << 563567;
	
	{
		CGU_SCOPE_INFO(test);
		CGU_LOG(warning)("test {}", CGU_CURRENT_SCOPE.fullname());
		CGU_LOG(trace)("trace on");
		CGU_LOG(critical)("oh no!");
	}

	log_scope_test::test();

	if (!glfwInit()) {
		CGU_LOG(critical)("Could not initialize GLFW");
		abort();
	}

	// GL 3.3 core context
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// disallow legacy (for OS X)
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	// request a debug context so we get debug callbacks
	// remove this for possible GL performance increases
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);

	// request a window that can perform gamma correction
	glfwWindowHint(GLFW_SRGB_CAPABLE, true);

	GLFWwindow *window = glfwCreateWindow(800, 600, "CGU", nullptr, nullptr);
	if (!window) {
		CGU_LOG(critical)("Could not create GLFW window");
		abort();
	}

	glfwMakeContextCurrent(window);

	glfwSwapInterval(0);

	// required for full GLEW functionality for GL3+
	glewExperimental = GL_TRUE;

	// initialize GLEW
	// must be done after making a GL context current
	GLenum glew_err = glewInit();
	if (GLEW_OK != glew_err) {
		CGU_LOG(critical)("Could not initialize GLEW: {}", glewGetErrorString(glew_err));
		abort();
	}

	// print out versions
	CGU_LOG(status)("OpenGL : {}", glGetString(GL_VERSION));

	// enable GL_ARB_debug_output if available
	if (glfwExtensionSupported("GL_ARB_debug_output")) {
		// this allows the error location to be determined from a stacktrace
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB);
		// setup up the callback
		glDebugMessageCallbackARB(gl_debug_callback, nullptr);
		glDebugMessageControlARB(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, true);
		CGU_LOG(trace)("GL_ARB_debug_output callback installed");
	} else {
		CGU_LOG(trace)("GL_ARB_debug_output not available");
	}

	// initialize ImGui
	imguictx = ImGui::CreateContext();
	ImGui::SetCurrentContext(imguictx);
	ImGui_ImplGlfw_InitForOpenGL(window, false);
	ImGui_ImplOpenGL3_Init();

	// attach input callbacks to window
	glfwSetCursorPosCallback(window, cursor_pos_callback);
	glfwSetMouseButtonCallback(window, mouse_button_callback);
	glfwSetScrollCallback(window, scroll_callback);
	glfwSetKeyCallback(window, key_callback);
	glfwSetCharCallback(window, char_callback);

	cgu::gl_mesh teapot = cgu::mesh_load_obj("./res/teapot.obj").build();

	cgu::profiler prof;
	cgu::profiler_gpu_resources profgpu;
	cgu::profile_task proftask{"display"};
	cgu::profile_collector profcollector{&prof};
	// TODO determine refresh rate from glfw?
	profcollector.data[&proftask].expected_duration = std::chrono::microseconds(16667);

	cgu::profile_task othertask{"stuff"};
	profcollector.data[&othertask];

	CGU_LOG(timing)("time since something: {}s", chrono::steady_clock::now().time_since_epoch() / 1.0s);

	// loop until the user closes the window
	while (!glfwWindowShouldClose(window)) {

		cgu::profile_frame_activation profactive{prof, proftask};
		cgu::profile_frame_binding profbinding{profactive, cgu::profile_flags::cpu_gpu, &profgpu};

		CGU_SCOPE_INFO(loop);
		CGU_PROFILE_CPU_GPU();

		{
			CGU_SCOPE_INFO(events);
			CGU_PROFILE_CPU();
			glfwPollEvents();
		}

		if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) glfwSetWindowShouldClose(window, true);

		glm::ivec2 fbsize;
		glfwGetFramebufferSize(window, &fbsize.x, &fbsize.y);
		if (fbsize.x == 0 || fbsize.y == 0) {
			this_thread::sleep_for(30ms);
			continue;
		}
		glViewport(0, 0, fbsize.x, fbsize.y);

		//CGU_LOG.info("framebuffer {}x{}", fbsize.x, fbsize.y).transient(true);

		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();

		{
			CGU_SCOPE_INFO(work);
			CGU_PROFILE_CPU();
			this_thread::sleep_for(2ms);
		}

		{
			CGU_SCOPE_INFO(render);
			CGU_PROFILE_CPU_GPU();

			glm::mat4 proj = glm::perspective(1.f, float(fbsize.x) / fbsize.y, 0.1f, zfar);
			glm::mat4 view = cam.view();

			{
				CGU_SCOPE_INFO(scene);
				CGU_PROFILE_CPU_GPU();

				fb_scene.bind();
				fb_scene.resize(fbsize);

				glClearColor(0.3f, 0.3f, 0.4f, 1.0f);
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
				glEnable(GL_DEPTH_TEST);
				glDepthFunc(GL_LEQUAL);

				cgu::use_dummy_shaderprog(glm::scale(view, glm::vec3(0.2f)), proj, zfar, {1, 1, 1, 1});
				glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
				teapot.draw();
				glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

				fb_scene.unbind();
			}

			{
				CGU_SCOPE_INFO(deferred);
				CGU_PROFILE_CPU_GPU();
				glDepthFunc(GL_ALWAYS);
				cgu::draw_texture2d(fb_scene[GL_COLOR_ATTACHMENT0].tex, fb_scene[GL_DEPTH_ATTACHMENT].tex, 0);
			}

			{
				CGU_SCOPE_INFO(forward);
				CGU_PROFILE_CPU_GPU();
				glDepthFunc(GL_LEQUAL);
				glEnable(GL_BLEND);
				glBlendEquation(GL_FUNC_ADD);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				cgu::draw_grid(view, proj, zfar);
				cgu::draw_axes(view, proj, zfar);
				glDisable(GL_BLEND);
			}

			glDisable(GL_DEPTH_TEST);
		}

		ImGui::ShowMetricsWindow();

		profcollector.poll();

		if (ImGui::Begin("Profiler")) {
			ImGui::draw_profile_collector(profcollector);
		}
		ImGui::End();

		if (ImGui::Begin("Logger")) {
			if (ImGui::Button("TEST")) CGU_LOG(debug)("TEST");
			imguilog.draw();
		}
		ImGui::End();

		ImGui::Render();
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

		{
			CGU_SCOPE_INFO(present);
			CGU_PROFILE_CPU_GPU();
			profgpu.pre_present();
			profgpu.try_wait_desktop_sync();
			glfwSwapBuffers(window);
			profgpu.post_present();
		}

		if (profcollector.want_clock_sync) {
			profgpu.update_clock_sync();
			profcollector.want_clock_sync = false;
		}

	}

	CGU_LOG(status)("exiting");

	glfwTerminate();

}

namespace {

	void cursor_pos_callback(GLFWwindow *win, double xpos, double ypos) {
		CGU_SCOPE_INFO(cursorpos);
		CGU_LOG(progress)("{},{}", xpos, ypos);
		// if not captured then foward to application
		ImGuiIO& io = ImGui::GetIO();
		if (io.WantCaptureMouse) return;
		glm::ivec2 winsize;
		glfwGetWindowSize(win, &winsize.x, &winsize.y);
		cam.update(glfwGetMouseButton(win, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS, {float(xpos), float(ypos)}, winsize);
	}

	void mouse_button_callback(GLFWwindow *win, int button, int action, int mods) {
		CGU_SCOPE_INFO(mousebutton);
		CGU_LOG(trace)("button={}", button);
		ImGui_ImplGlfw_MouseButtonCallback(win, button, action, mods);
		// if not captured then foward to application
		ImGuiIO& io = ImGui::GetIO();
		if (io.WantCaptureMouse) return;
	}

	void scroll_callback(GLFWwindow *win, double xoffset, double yoffset) {
		ImGui_ImplGlfw_ScrollCallback(win, xoffset, yoffset);
		// if not captured then foward to application
		ImGuiIO& io = ImGui::GetIO();
		if (io.WantCaptureMouse) return;
		cam.zoom(float(yoffset));
	}

	void key_callback(GLFWwindow *win, int key, int scancode, int action, int mods) {
		ImGui_ImplGlfw_KeyCallback(win, key, scancode, action, mods);
		// if not captured then foward to application
		ImGuiIO& io = ImGui::GetIO();
		if (io.WantCaptureKeyboard) return;
	}

	void char_callback(GLFWwindow *win, unsigned int c) {
		ImGui_ImplGlfw_CharCallback(win, c);
		// if not captured then foward to application
		ImGuiIO& io = ImGui::GetIO();
		if (io.WantTextInput) return;
	}

	void APIENTRY gl_debug_callback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei, const GLchar *message, const GLvoid *) {
		// don't report notification messages
		if (severity == GL_DEBUG_SEVERITY_NOTIFICATION) return;

		// nvidia: avoid debug spam about attribute offsets
		if (id == 131076) return;

		cerr << "GL [" << cgu::gl_debug_source_string(source) << "] " << cgu::gl_debug_type_string(type) << ' ' << id << " : ";
		cerr << message << " (Severity: " << cgu::gl_debug_severity_string(severity) << ')' << endl;

		if (type == GL_DEBUG_TYPE_ERROR_ARB) throw runtime_error("GL Error: "s + message);
	}

}

CGU_DEFINE_SCOPE_INFO_CLASS(log_scope_test, class_scope);

void log_scope_test::test() {
	CGU_LOG(debug)("class scope test?");
}
